﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace WindowsService1
{
    public partial class Service1 : ServiceBase
    {
        static string connectionString = "SERVER = localhost; DATABASE = pengrasi; USER ID = sa; PASSWORD = 123; MultipleActiveResultSets=True";
        static SqlConnection sqlconn = new SqlConnection(connectionString);

        private System.Timers.Timer timer = null;
        public Service1()
        {
            InitializeComponent();
        }
        
        protected override void OnStart(string[] args)
        {
            timer = new System.Timers.Timer(5000);
            timer.Elapsed += Time_tick;
            timer.AutoReset = true;
            timer.Enabled = true;

        }

        private void Time_tick(object sender, ElapsedEventArgs e)
        {
            Process.Start(@"C:\NawaData\FinalProjectGitlab\PengRaSi\ConsoleApp1\ConsoleApp1\bin\Debug\ConsoleApp1");

        }

        protected override void OnStop()
        {
            timer.Stop();
            timer = null;
        }
    }
}
