﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PengRaSi.Controllers
{
    public class NoteHomeroomController : Controller
    {
        pengrasiEntities db = new pengrasiEntities();
        // GET: NoteHomeroom
        public ActionResult Index(int id)
        {
            var idTeach = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).First();
            var idClass = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == idTeach).Select(x => x.FK_ID_CLASS).First();
            var studentView = db.HomeroomNotes.Where(x => x.FK_ID_CLASS == idClass && (x.SMT==id ||x.SMT==null));
            ViewData["SMT"] = id;
            ViewData["Class"] = db.TBL_CLASS.Where(x => x.ID_CLASS == idClass).Select(x => x.CLASS_NAME).FirstOrDefault();
            return View(studentView);
        }

        public ActionResult SMTSel()
        {
            var idTeach = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).First();
            var idClass = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == idTeach).Select(x => x.FK_ID_CLASS).First();
            var smtList = db.TBL_SUBJECT_CLASS.Where(x => x.FK_ID_CLASS == idClass).AsEnumerable().GroupBy(x => x.SMT).Select(x => x.First());
            ViewBag.SMT = new SelectList(smtList, "SMT", "SMT");
            return View();
        }
        [HttpPost, ActionName("SMTSel")]
        public ActionResult SMTPost(int SMT)
        {
            //var SMT = Request["SMT"].ToString();
            return RedirectToAction("Index", "NoteHomeroom", new { id = SMT });
        }

        public ActionResult Create(string id, int smt)
        {
            TBL_NOTE_HOMEROOM tBL_NOTE_HOMEROOM = new TBL_NOTE_HOMEROOM
            {
                FK_ID_STUDENT = id,
                SMT = smt
            };
            ViewData["Student"] = db.TBL_STUDENT.Where(x => x.ID_STUDENT == id).Select(x => x.STUDENT_NAME).FirstOrDefault();

            return View(tBL_NOTE_HOMEROOM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FK_ID_STUDENT, NOTE_HOMEROOM, SMT")] TBL_NOTE_HOMEROOM tBL_NOTE_HOMEROOM)
        {
            if(ModelState.IsValid)
            {
                db.TBL_NOTE_HOMEROOM.Add(tBL_NOTE_HOMEROOM);
                db.SaveChanges();

                return RedirectToAction("Index", "NoteHomeroom", new { id = tBL_NOTE_HOMEROOM.SMT });
            }
            return View(tBL_NOTE_HOMEROOM);
        }


        public ActionResult Edit(string id, int smt)
        {
            TBL_NOTE_HOMEROOM tBL_NOTE_HOMEROOM = db.TBL_NOTE_HOMEROOM.Where(x => x.FK_ID_STUDENT == id && x.SMT == smt).FirstOrDefault();
            if(tBL_NOTE_HOMEROOM == null)
            {
                return RedirectToAction("Create", new { id, smt });
            }
            ViewData["Student"] = db.TBL_STUDENT.Where(x => x.ID_STUDENT == id).Select(x => x.STUDENT_NAME).FirstOrDefault();
            return View(tBL_NOTE_HOMEROOM);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_NOTE_HOMEROOM, FK_ID_STUDENT, NOTE_HOMEROOM, SMT")] TBL_NOTE_HOMEROOM tBL_NOTE_HOMEROOM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tBL_NOTE_HOMEROOM).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", "NoteHomeroom", new { id = tBL_NOTE_HOMEROOM.SMT });
            }
            return View(tBL_NOTE_HOMEROOM);
        }

    }
}