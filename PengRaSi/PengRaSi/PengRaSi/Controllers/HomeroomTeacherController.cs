﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class HomeroomTeacherController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: HomeroomTeacher
        public ActionResult Index()
        {
            var tBL_HOMEROOM = db.TBL_HOMEROOM.Include(t => t.TBL_CLASS).Include(t => t.TBL_TEACHER);
            return View(tBL_HOMEROOM.ToList());
        }

        // GET: HomeroomTeacher/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_HOMEROOM tBL_HOMEROOM = db.TBL_HOMEROOM.Find(id);
            if (tBL_HOMEROOM == null)
            {
                return HttpNotFound();
            }
            return View(tBL_HOMEROOM);
        }

        // GET: HomeroomTeacher/Create
        public ActionResult Create()
        {
            string currentYear = DateTime.Now.Year.ToString();
            int currentyearint = Convert.ToInt32(currentYear);

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER.Where(x => x.TEACHER_NAME != "ADMIN"), "ID_TEACHER", "TEACHER_NAME");
            return View();
        }

        // POST: HomeroomTeacher/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_HOMEROOM,FK_ID_TEACHER,FK_ID_CLASS")] TBL_HOMEROOM tBL_HOMEROOM)
        {
            string a = tBL_HOMEROOM.FK_ID_TEACHER;
            string b = tBL_HOMEROOM.FK_ID_CLASS;
            TBL_HOMEROOM tb_hoomroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == a).FirstOrDefault();
            TBL_HOMEROOM tb_hoomroom1 = db.TBL_HOMEROOM.Where(x => x.FK_ID_CLASS == b).FirstOrDefault();

            if(tb_hoomroom == null)
            {
                if(tb_hoomroom1 == null)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            db.TBL_HOMEROOM.Add(tBL_HOMEROOM);
                            db.SaveChanges();
                            ts.Commit();
                            return RedirectToAction("Index");
                        }
                        catch (Exception)
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }
                else
                {
                    ViewData["class"] = "Classes Cannot Be The Same ";
                }
            }
            else
            {
                ViewData["teacher"] = "Teacher Already Exist";
            }

            
            string currentYear = DateTime.Now.Year.ToString();
            int currentyearint = Convert.ToInt32(currentYear);
            //ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS.Where(x=>x.SCH_YEAR == currentyearint).ToList(), "ID_CLASS", "CLASS_NAME", tBL_HOMEROOM.FK_ID_CLASS);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_HOMEROOM.FK_ID_CLASS);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER.Where(x=>x.TEACHER_NAME!="ADMIN" ), "ID_TEACHER", "TEACHER_NAME", tBL_HOMEROOM.FK_ID_TEACHER);
            return View(tBL_HOMEROOM);
        }

        // GET: HomeroomTeacher/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_HOMEROOM tBL_HOMEROOM = db.TBL_HOMEROOM.Find(id);
            if (tBL_HOMEROOM == null)
            {
                return HttpNotFound();
            }
            string currentYear = DateTime.Now.Year.ToString();
            int currentyearint = Convert.ToInt32(currentYear);
            //ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS.Where(x => x.SCH_YEAR == currentyearint).ToList(), "ID_CLASS", "CLASS_NAME", tBL_HOMEROOM.FK_ID_CLASS);

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            //ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_HOMEROOM.FK_ID_CLASS);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_HOMEROOM.FK_ID_TEACHER);
            return View(tBL_HOMEROOM);
        }

        // POST: HomeroomTeacher/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_HOMEROOM,FK_ID_TEACHER,FK_ID_CLASS")] TBL_HOMEROOM tBL_HOMEROOM)
        {
            string b = tBL_HOMEROOM.FK_ID_CLASS;
            TBL_HOMEROOM tb_hoomroom1 = db.TBL_HOMEROOM.Where(x => x.FK_ID_CLASS == b).FirstOrDefault();
            if(tb_hoomroom1 == null)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.Entry(tBL_HOMEROOM).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }

            }
            else
            {
                ViewData["class"] = "Classes cannot Be The Same Or Already ";
            }

            //if (ModelState.IsValid)
            //{

            //}
            string currentYear = DateTime.Now.Year.ToString();
            int currentyearint = Convert.ToInt32(currentYear);
            //ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS.Where(x => x.SCH_YEAR == currentyearint).ToList(), "ID_CLASS", "CLASS_NAME", tBL_HOMEROOM.FK_ID_CLASS);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_HOMEROOM.FK_ID_CLASS);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_HOMEROOM.FK_ID_TEACHER);
            return View(tBL_HOMEROOM);
        }

        // GET: HomeroomTeacher/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_HOMEROOM tBL_HOMEROOM = db.TBL_HOMEROOM.Find(id);
            if (tBL_HOMEROOM == null)
            {
                return HttpNotFound();
            }
            return View(tBL_HOMEROOM);
        }

        // POST: HomeroomTeacher/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            var getIdHomeroomTeacher = db.TBL_HOMEROOM.Where(x => x.ID_HOMEROOM == id).FirstOrDefault();
            var getIdTeacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == getIdHomeroomTeacher.FK_ID_TEACHER).FirstOrDefault();
            var getUserId = db.TBL_USER.Where(x => x.FK_ID_TEACHER == getIdTeacher.ID_TEACHER).FirstOrDefault();

            if(getUserId.POSITION== "Homeroom Teacher")
            {
                ViewData["toteacher"] = "Please Change Position Homeroom Teacher At Menu Manage User, Name: " +getIdTeacher.TEACHER_NAME;
            }
            else
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        TBL_HOMEROOM tBL_HOMEROOM = db.TBL_HOMEROOM.Find(id);
                        db.TBL_HOMEROOM.Remove(tBL_HOMEROOM);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
            }


           
            return View();
                   
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
