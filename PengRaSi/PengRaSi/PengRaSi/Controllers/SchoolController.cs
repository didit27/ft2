﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PengRaSi.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SchoolController : Controller
    {
        pengrasiEntities db = new pengrasiEntities();
        // GET: School
        public ActionResult Index()
        {
            return RedirectToAction("Index","Home");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include ="SCHOOL_NAME, SCHOOL_ADDRESS")] MS_SCHOOL mS_SCHOOL, HttpPostedFileBase postedFile)
        {
            byte[] bytes;
            using (BinaryReader br = new BinaryReader(postedFile.InputStream))
            {
                bytes = br.ReadBytes(postedFile.ContentLength);
            }

            mS_SCHOOL.SCHOOL_LOGO = bytes;

            if(ModelState.IsValid)
            {
                db.MS_SCHOOL.Add(mS_SCHOOL);
                db.SaveChanges();

                return RedirectToAction("Index", "Home");
            }

            return View(mS_SCHOOL);
        }



        public ActionResult Edit()
        {

            var tb_Sch = db.MS_SCHOOL.FirstOrDefault();

            if (tb_Sch == null)
            {
                return RedirectToAction("Create");
            }
            
            return View(tb_Sch);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include ="ID_SCHOOL, SCHOOL_NAME, SCHOOL_ADDRESS, SCHOOL_LOGO")] MS_SCHOOL mS_SCHOOL, HttpPostedFileBase postedFile)
        {
            byte[] bytes;
            if (postedFile!=null)
            {
                using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                {
                    bytes = br.ReadBytes(postedFile.ContentLength);
                }
                mS_SCHOOL.SCHOOL_LOGO = bytes;
            }
            

            if(ModelState.IsValid)
            {
                db.Entry(mS_SCHOOL).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(mS_SCHOOL);
        }
    }

    
}