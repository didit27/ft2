﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PengRaSi.Controllers
{
    public class ScoreController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        public string getPredikat(double grade, int menentukansmster)
        {
            string PREDIKAT = "";
            if (menentukansmster == 1)
            {
                if (grade >= 0 && grade <= 74)
                {
                    PREDIKAT = "D";
                }
                else if (grade >= 75 && grade <= 82)
                {
                    PREDIKAT = "C";
                }
                else if (grade >= 83 && grade <= 90)
                {
                    PREDIKAT = "B";
                }
                else if (grade >= 91 && grade <= 100)
                {
                    PREDIKAT = "A";
                }
            }
            else if (menentukansmster == 2)
            {
                if (grade >= 0 && grade <= 75)
                {
                    PREDIKAT = "D";
                }
                else if (grade >= 76 && grade <= 83)
                {
                    PREDIKAT = "C";
                }
                else if (grade >= 84 && grade <= 91)
                {
                    PREDIKAT = "B";
                }
                else if (grade >= 92 && grade <= 100)
                {
                    PREDIKAT = "A";
                }
            }
            else if (menentukansmster == 3)
            {
                if (grade >= 0 && grade <= 76)
                {
                    PREDIKAT = "D";
                }
                else if (grade >= 77 && grade <= 84)
                {
                    PREDIKAT = "C";
                }
                else if (grade >= 85 && grade <= 92)
                {
                    PREDIKAT = "B";
                }
                else if (grade >= 93 && grade <= 100)
                {
                    PREDIKAT = "A";
                }
            }
            else if (menentukansmster == 4)
            {
                if (grade >= 0 && grade <= 77)
                {
                    PREDIKAT = "D";
                }
                else if (grade >= 78 && grade <= 84)
                {
                    PREDIKAT = "C";
                }
                else if (grade >= 85 && grade <= 92)
                {
                    PREDIKAT = "B";
                }
                else if (grade >= 93 && grade <= 100)
                {
                    PREDIKAT = "A";
                }
            }
            else if (menentukansmster == 5)
            {
                if (grade >= 0 && grade <= 78)
                {
                    PREDIKAT = "D";
                }
                else if (grade >= 79 && grade <= 85)
                {
                    PREDIKAT = "C";
                }
                else if (grade >= 86 && grade <= 92)
                {
                    PREDIKAT = "B";
                }
                else if (grade >= 93 && grade <= 100)
                {
                    PREDIKAT = "A";
                }
            }
            else if (menentukansmster == 6)
            {
                if (grade >= 0 && grade <= 79)
                {
                    PREDIKAT = "D";
                }
                else if (grade >= 80 && grade <= 86)
                {
                    PREDIKAT = "C";
                }
                else if (grade >= 87 && grade <= 93)
                {
                    PREDIKAT = "B";
                }
                else if (grade >= 94 && grade <= 100)
                {
                    PREDIKAT = "A";
                }
            }

            return PREDIKAT;
        }

        // GET: Score
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Insert(string id, int idSubj)
        {
            var cekScore = db.TBL_SCORE.Where(x => x.FK_ID_STUDENT == id && x.FK_ID_SUBJECT_CLASS == idSubj).FirstOrDefault();
            if(cekScore == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["studentname"] = cekScore.TBL_STUDENT.STUDENT_NAME;
            }
          


            if (cekScore != null)
            {
                return View(cekScore);
            }

            TBL_SCORE tbl_Score = new TBL_SCORE
            {
                FK_ID_STUDENT = id,
                FK_ID_SUBJECT_CLASS = idSubj
            };

            return View(tbl_Score);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert([Bind(Include = "ID_SCORE, SCORE, FK_ID_STUDENT, FK_ID_SUBJECT_CLASS,PREDIKAT")] TBL_SCORE tBL_SCORE)
        {
            var cekScore = db.TBL_SCORE.Where(x => x.FK_ID_STUDENT == tBL_SCORE.FK_ID_STUDENT && x.FK_ID_SUBJECT_CLASS == tBL_SCORE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
            

            double grade = Math.Round(Convert.ToDouble(tBL_SCORE.SCORE));
            double score_range = grade;
            if (score_range >= 0 && score_range <= 100)
            {
                TBL_SUBJECT_CLASS semester = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == tBL_SCORE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
                int menentukansmster = Convert.ToInt32(semester.SMT);
                tBL_SCORE.PREDIKAT = getPredikat(grade, menentukansmster);

                if (ModelState.IsValid)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (cekScore != null)
                            {
                                db.TBL_SCORE.Remove(cekScore);
                                db.SaveChanges();
                                //ts.Commit();
                            }
                            db.TBL_SCORE.Add(tBL_SCORE);
                            db.SaveChanges();
                            ts.Commit();

                            return RedirectToAction("IndexTeacher", "Student", routeValues: new { id = tBL_SCORE.FK_ID_SUBJECT_CLASS });
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }
                return View(tBL_SCORE);
            }
            else
            {
                ViewData["Range"] = "Score 0 Up To 100";
            }
            return View();
           
        }


        public ActionResult InsertAtitude(string id, int idSubj)
        {
            var cekScore = db.TBL_SCORE_ATTITUDE.Where(x => x.FK_ID_STUDENT == id && x.FK_ID_SUBJECT_CLASS == idSubj).FirstOrDefault();
            if (cekScore == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["studentname"] = cekScore.TBL_STUDENT.STUDENT_NAME;
            }


            if (cekScore != null)
            {
                return View(cekScore);
            }

            TBL_SCORE_ATTITUDE tbl_ScoreAtitude = new TBL_SCORE_ATTITUDE
            {
                FK_ID_STUDENT = id,
                FK_ID_SUBJECT_CLASS = idSubj
            };

            return View(tbl_ScoreAtitude);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertAtitude([Bind(Include = "ID_SCORE, SCORE, FK_ID_STUDENT, FK_ID_SUBJECT_CLASS,PREDIKAT")] TBL_SCORE_ATTITUDE tBL_SCORE_ATITUDE)
        {
            var cekScoreAtitude = db.TBL_SCORE_ATTITUDE.Where(x => x.FK_ID_STUDENT == tBL_SCORE_ATITUDE.FK_ID_STUDENT && x.FK_ID_SUBJECT_CLASS == tBL_SCORE_ATITUDE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
            


            double grade = Math.Round(Convert.ToDouble(tBL_SCORE_ATITUDE.SCORE));
            double score_range = grade;
            if (score_range >= 0 && score_range <= 100)
            {
                TBL_SUBJECT_CLASS semester = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == tBL_SCORE_ATITUDE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
                int menentukansmster = Convert.ToInt32(semester.SMT);
                tBL_SCORE_ATITUDE.PREDIKAT = getPredikat(grade, menentukansmster);
                
                if (ModelState.IsValid)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (cekScoreAtitude != null)
                            {
                                db.TBL_SCORE_ATTITUDE.Remove(cekScoreAtitude);
                                db.SaveChanges();
                                //ts.Commit();
                            }
                            db.TBL_SCORE_ATTITUDE.Add(tBL_SCORE_ATITUDE);
                            db.SaveChanges();
                            ts.Commit();

                            return RedirectToAction("IndexTeacherScoreAtitude", "Student", routeValues: new { id = tBL_SCORE_ATITUDE.FK_ID_SUBJECT_CLASS });
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }
                return View(tBL_SCORE_ATITUDE);
            }
            else
            {
                ViewData["Range"] = "Score 0 Up To 100";
            }
            return View();

        }


        public ActionResult InsertPractices(string id, int idSubj)
        {
            var cekScore = db.TBL_SCORE_PRACTICE.Where(x => x.FK_ID_STUDENT == id && x.FK_ID_SUBJECT_CLASS == idSubj).FirstOrDefault();
            if (cekScore == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["studentname"] = cekScore.TBL_STUDENT.STUDENT_NAME;
            }


            if (cekScore != null)
            {
                return View(cekScore);
            }

            TBL_SCORE_PRACTICE tbl_ScorePractice = new TBL_SCORE_PRACTICE
            {
                FK_ID_STUDENT = id,
                FK_ID_SUBJECT_CLASS = idSubj
            };

            return View(tbl_ScorePractice);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertPractices([Bind(Include = "ID_SCORE, SCORE, FK_ID_STUDENT, FK_ID_SUBJECT_CLASS,PREDIKAT")] TBL_SCORE_PRACTICE tBL_SCORE_PRATICE)
        {
            var cekScorePratice = db.TBL_SCORE_PRACTICE.Where(x => x.FK_ID_STUDENT == tBL_SCORE_PRATICE.FK_ID_STUDENT && x.FK_ID_SUBJECT_CLASS == tBL_SCORE_PRATICE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
            
            double grade = Math.Round(Convert.ToDouble(tBL_SCORE_PRATICE.SCORE));
            double score_range = grade;
            if (score_range >= 0 && score_range <= 100)
            {
                TBL_SUBJECT_CLASS semester = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == tBL_SCORE_PRATICE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
                int menentukansmster = Convert.ToInt32(semester.SMT);
                tBL_SCORE_PRATICE.PREDIKAT = getPredikat(grade, menentukansmster);
                
                if (ModelState.IsValid)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (cekScorePratice != null)
                            {
                                db.TBL_SCORE_PRACTICE.Remove(cekScorePratice);
                                db.SaveChanges();
                                //ts.Commit();
                            }
                            db.TBL_SCORE_PRACTICE.Add(tBL_SCORE_PRATICE);
                            db.SaveChanges();
                            ts.Commit();

                            return RedirectToAction("IndexTeacherScorePractices", "Student", routeValues: new { id = tBL_SCORE_PRATICE.FK_ID_SUBJECT_CLASS });
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }
                return View(tBL_SCORE_PRATICE);
            }
            else
            {
                ViewData["Range"] = "Score 0 Up To 100";
            }
            return View();

        }

        public ActionResult InsertHRTheory(string id, int idSubj)
        {
            var cekScore = db.TBL_SCORE.Where(x => x.FK_ID_STUDENT == id && x.FK_ID_SUBJECT_CLASS == idSubj).FirstOrDefault();
            if (cekScore == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["studentname"] = cekScore.TBL_STUDENT.STUDENT_NAME;
            }


            if (cekScore != null)
            {
                return View(cekScore);
            }

            TBL_SCORE tbl_Score = new TBL_SCORE
            {
                FK_ID_STUDENT = id,
                FK_ID_SUBJECT_CLASS = idSubj
            };

            return View(tbl_Score);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertHRTheory([Bind(Include = "ID_SCORE, SCORE, FK_ID_STUDENT, FK_ID_SUBJECT_CLASS,PREDIKAT")] TBL_SCORE tBL_SCORE)
        {
            var cekScore = db.TBL_SCORE.Where(x => x.FK_ID_STUDENT == tBL_SCORE.FK_ID_STUDENT && x.FK_ID_SUBJECT_CLASS == tBL_SCORE.FK_ID_SUBJECT_CLASS).FirstOrDefault();

            double grade = Math.Round(Convert.ToDouble(tBL_SCORE.SCORE));
            double score_range = grade;
            if (score_range >= 0 && score_range <= 100)
            {
                TBL_SUBJECT_CLASS semester = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == tBL_SCORE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
                int menentukansmster = Convert.ToInt32(semester.SMT);
                tBL_SCORE.PREDIKAT = getPredikat(grade, menentukansmster);

                if (ModelState.IsValid)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (cekScore != null)
                            {
                                db.TBL_SCORE.Remove(cekScore);
                                db.SaveChanges();
                                //ts.Commit();
                            }
                            db.TBL_SCORE.Add(tBL_SCORE);
                            db.SaveChanges();
                            ts.Commit();

                            return RedirectToAction("IndexHRScoreTheory", "Student", routeValues: new { id = tBL_SCORE.FK_ID_SUBJECT_CLASS });
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }
                return View(tBL_SCORE);
            }
            else
            {
                ViewData["Range"] = "Score 0 Up To 100";
            }
            return View();

        }

        public ActionResult InsertHRAtitude(string id, int idSubj)
        {
            var cekScore = db.TBL_SCORE_ATTITUDE.Where(x => x.FK_ID_STUDENT == id && x.FK_ID_SUBJECT_CLASS == idSubj).FirstOrDefault();
            if (cekScore == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["studentname"] = cekScore.TBL_STUDENT.STUDENT_NAME;
            }


            if (cekScore != null)
            {
                return View(cekScore);
            }

            TBL_SCORE_ATTITUDE tbl_ScoreAtitude = new TBL_SCORE_ATTITUDE
            {
                FK_ID_STUDENT = id,
                FK_ID_SUBJECT_CLASS = idSubj
            };

            return View(tbl_ScoreAtitude);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertHRAtitude([Bind(Include = "ID_SCORE, SCORE, FK_ID_STUDENT, FK_ID_SUBJECT_CLASS,PREDIKAT")] TBL_SCORE_ATTITUDE tBL_SCORE_ATITUDE)
        {
            var cekScoreAtitude = db.TBL_SCORE_ATTITUDE.Where(x => x.FK_ID_STUDENT == tBL_SCORE_ATITUDE.FK_ID_STUDENT && x.FK_ID_SUBJECT_CLASS == tBL_SCORE_ATITUDE.FK_ID_SUBJECT_CLASS).FirstOrDefault();



            double grade = Math.Round(Convert.ToDouble(tBL_SCORE_ATITUDE.SCORE));
            double score_range = grade;
            if (score_range >= 0 && score_range <= 100)
            {
                TBL_SUBJECT_CLASS semester = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == tBL_SCORE_ATITUDE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
                int menentukansmster = Convert.ToInt32(semester.SMT);
                tBL_SCORE_ATITUDE.PREDIKAT = getPredikat(grade, menentukansmster);

                if (ModelState.IsValid)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (cekScoreAtitude != null)
                            {
                                db.TBL_SCORE_ATTITUDE.Remove(cekScoreAtitude);
                                db.SaveChanges();
                                //ts.Commit();
                            }
                            db.TBL_SCORE_ATTITUDE.Add(tBL_SCORE_ATITUDE);
                            db.SaveChanges();
                            ts.Commit();

                            return RedirectToAction("IndexHRScoreAtitude", "Student", routeValues: new { id = tBL_SCORE_ATITUDE.FK_ID_SUBJECT_CLASS });
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }
                return View(tBL_SCORE_ATITUDE);
            }
            else
            {
                ViewData["Range"] = "Score 0 Up To 100";
            }
            return View();

        }

        public ActionResult InsertHRPractices(string id, int idSubj)
        {
            var cekScore = db.TBL_SCORE_PRACTICE.Where(x => x.FK_ID_STUDENT == id && x.FK_ID_SUBJECT_CLASS == idSubj).FirstOrDefault();
            if (cekScore == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["studentname"] = cekScore.TBL_STUDENT.STUDENT_NAME;
            }


            if (cekScore != null)
            {
                return View(cekScore);
            }

            TBL_SCORE_PRACTICE tbl_ScorePractice = new TBL_SCORE_PRACTICE
            {
                FK_ID_STUDENT = id,
                FK_ID_SUBJECT_CLASS = idSubj
            };

            return View(tbl_ScorePractice);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertHRPractices([Bind(Include = "ID_SCORE, SCORE, FK_ID_STUDENT, FK_ID_SUBJECT_CLASS,PREDIKAT")] TBL_SCORE_PRACTICE tBL_SCORE_PRATICE)
        {
            var cekScorePratice = db.TBL_SCORE_PRACTICE.Where(x => x.FK_ID_STUDENT == tBL_SCORE_PRATICE.FK_ID_STUDENT && x.FK_ID_SUBJECT_CLASS == tBL_SCORE_PRATICE.FK_ID_SUBJECT_CLASS).FirstOrDefault();

            double grade = Math.Round(Convert.ToDouble(tBL_SCORE_PRATICE.SCORE));
            double score_range = grade;
            if (score_range >= 0 && score_range <= 100)
            {
                TBL_SUBJECT_CLASS semester = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == tBL_SCORE_PRATICE.FK_ID_SUBJECT_CLASS).FirstOrDefault();
                int menentukansmster = Convert.ToInt32(semester.SMT);
                tBL_SCORE_PRATICE.PREDIKAT = getPredikat(grade, menentukansmster);

                if (ModelState.IsValid)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (cekScorePratice != null)
                            {
                                db.TBL_SCORE_PRACTICE.Remove(cekScorePratice);
                                db.SaveChanges();
                                //ts.Commit();
                            }
                            db.TBL_SCORE_PRACTICE.Add(tBL_SCORE_PRATICE);
                            db.SaveChanges();
                            ts.Commit();

                            return RedirectToAction("IndexHRScorePractices", "Student", routeValues: new { id = tBL_SCORE_PRATICE.FK_ID_SUBJECT_CLASS });
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }
                return View(tBL_SCORE_PRATICE);
            }
            else
            {
                ViewData["Range"] = "Score 0 Up To 100";
            }
            return View();

        }

    }
}