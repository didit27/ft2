﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;
using System.IO;

namespace PengRaSi.Controllers
{
    public class SignatureController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        public ActionResult Index()
        {
            var tBL_SIGNATURE = db.TBL_SIGNATURE.Include(t => t.TBL_TEACHER);
            return View(tBL_SIGNATURE.ToList());
        }

        

        // GET: Signature/Create
        public ActionResult Create()
        {
            var id_teacher = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).FirstOrDefault();
            TBL_SIGNATURE tBL_SIGNATURE = new TBL_SIGNATURE
            {
                FK_ID_TEACHER = id_teacher
            };
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME");
            return View(tBL_SIGNATURE);
        }

        // POST: Signature/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_SIGNATURE,FK_ID_TEACHER")] TBL_SIGNATURE tBL_SIGNATURE, HttpPostedFileBase postedFile)
        {
            byte[] bytes;
            using (BinaryReader br = new BinaryReader(postedFile.InputStream))
            {
                bytes = br.ReadBytes(postedFile.ContentLength);
            }
            tBL_SIGNATURE.SIGNATURE = bytes;
            if (ModelState.IsValid)
            {
                db.TBL_SIGNATURE.Add(tBL_SIGNATURE);
                db.SaveChanges();
                TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).FirstOrDefault();
                if (tb_user.POSITION == "Administrator")
                {
                    return RedirectToAction("Index", "Home");
                }
                if (tb_user.POSITION == "Homeroom Teacher")
                {
                    return RedirectToAction("HomeroomTeacher", "Test");
                }
                if (tb_user.POSITION == "Teacher")
                {
                    return RedirectToAction("teacher", "Test");
                }
                
            }

            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_SIGNATURE.FK_ID_TEACHER);
            return View(tBL_SIGNATURE);
        }

        // GET: Signature/Edit/5
        public ActionResult Edit()
        {
            var id_teacher = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).FirstOrDefault();
            if (id_teacher == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var tBL_SIGNATURE = db.TBL_SIGNATURE.Where(x=>x.FK_ID_TEACHER == id_teacher).FirstOrDefault();

            if (tBL_SIGNATURE == null)
            {
                return RedirectToAction("Create");
            }
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_SIGNATURE.FK_ID_TEACHER);
            return View(tBL_SIGNATURE);
        }

        // POST: Signature/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_SIGNATURE,SIGNATURE,FK_ID_TEACHER")] TBL_SIGNATURE tBL_SIGNATURE, HttpPostedFileBase postedFile)
        {
            byte[] bytes;
            using (BinaryReader br = new BinaryReader(postedFile.InputStream))
            {
                bytes = br.ReadBytes(postedFile.ContentLength);
            }
            tBL_SIGNATURE.SIGNATURE = bytes;
            if (ModelState.IsValid)
            {
                db.Entry(tBL_SIGNATURE).State = EntityState.Modified;
                db.SaveChanges();
                TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).FirstOrDefault();
                if (tb_user.POSITION == "Administrator")
                {
                    return RedirectToAction("Index", "Home");
                }
                if (tb_user.POSITION == "Homeroom Teacher")
                {
                    return RedirectToAction("HomeroomTeacher", "Test");
                }
                if (tb_user.POSITION == "Teacher")
                {
                    return RedirectToAction("teacher", "Test");
                }
            }
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_SIGNATURE.FK_ID_TEACHER);
            return View(tBL_SIGNATURE);
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
