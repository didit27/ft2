﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PengRaSi.Controllers
{
    public class TestController : Controller
    {
        pengrasiEntities db = new pengrasiEntities();
        // GET: Test
        [Authorize(Roles = "Teacher,Homeroom Teacher,Administrator")]
        public ActionResult Teacher()
        {
            var tbUser = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).FirstOrDefault();
            var tbTeacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tbUser.FK_ID_TEACHER).FirstOrDefault();
            var tbSubj = db.TBL_SUBJECT_CLASS.Where(x => x.FK_ID_TEACHER == tbTeacher.ID_TEACHER);
           
            ViewBag.Teacher = tbTeacher;
            ViewData["Subject"] = tbSubj;
            ViewBag.ListSubject = tbSubj.AsEnumerable().GroupBy(x => x.FK_ID_SUBJECT).Select(x => x.First());
            return View(tbSubj);
        }
        [Authorize(Roles = "Homeroom Teacher,Administrator")]
        public ActionResult HomeroomTeacher()
        {
          
            var Notifdt = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");

            ViewData["time"] = Notifdt;
            return View();
        }
    }
}