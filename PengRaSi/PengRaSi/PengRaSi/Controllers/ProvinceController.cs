﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class ProvinceController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Province
        public ActionResult Index()
        {
            return View(db.MS_PROV.ToList());
        }

       

        // GET: Province/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Province/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_PROV,PROVINCE")] MS_PROV mS_PROV)
        {
            if (ModelState.IsValid)
            {
                db.MS_PROV.Add(mS_PROV);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mS_PROV);
        }

        

        // GET: Province/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_PROV mS_PROV = db.MS_PROV.Find(id);
            db.MS_PROV.Remove(mS_PROV);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
