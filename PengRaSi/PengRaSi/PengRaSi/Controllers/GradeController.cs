﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;


namespace PengRaSi.Controllers
{
    [RoutePrefix("")]
    
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class GradeController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();
        Models.Encryption code = new Models.Encryption();
        // GET: Grade

        public ActionResult Index()
        {
            return View(db.MS_GRADE.ToList());
        }
       
        // GET: Grade/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_GRADE mS_GRADE = db.MS_GRADE.Find(id);
            if (mS_GRADE == null)
            {
                return HttpNotFound();
            }
            return View(mS_GRADE);
        }

        // GET: Grade/Create
        public ActionResult Create()
        {
            return View();
        }
       
        // POST: Grade/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_GRADE,GRADE")] MS_GRADE mS_GRADE)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.MS_GRADE.Add(mS_GRADE);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception )
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }    
            }

            return View(mS_GRADE);
        }
        
        // GET: Grade/Edit/5
        
        public ActionResult Edit(int? id)
        {
           
            if (id == null)
            {
                
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_GRADE mS_GRADE = db.MS_GRADE.Find(id);
            //string idstring = Convert.ToString(mS_GRADE.ID_GRADE);
            //string dec = code.Encrypt(idstring);

            
            //mS_GRADE.ID_GRADE = Convert.ToInt32(dec);
           
            if (mS_GRADE == null)
            {
                return HttpNotFound();
            }
            return View(mS_GRADE);
        }
        
        // POST: Grade/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_GRADE,GRADE")] MS_GRADE mS_GRADE)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        mS_GRADE.ID_GRADE = Convert.ToInt32(code.Decrypt(Convert.ToString(mS_GRADE.ID_GRADE)));
                        db.Entry(mS_GRADE).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }     
            }
            return View(mS_GRADE);
        }
        
        // GET: Grade/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_GRADE mS_GRADE = db.MS_GRADE.Find(id);
            if (mS_GRADE == null)
            {
                return HttpNotFound();
            }
            return View(mS_GRADE);
        }
        
        // POST: Grade/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    MS_GRADE mS_GRADE = db.MS_GRADE.Find(id);
                    db.MS_GRADE.Remove(mS_GRADE);

                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();
          
        }
       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
