﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class CityController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: City
        public ActionResult Index()
        {
            var mS_CITY = db.MS_CITY.Include(m => m.MS_PROV);
            return View(mS_CITY.ToList());
        }

        

        // GET: City/Create
        public ActionResult Create()
        {
            ViewBag.FK_ID_PROVENCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE");
            return View();
        }

        // POST: City/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_CITY,CITY,FK_ID_PROVENCE")] MS_CITY mS_CITY)
        {
            if (ModelState.IsValid)
            {
                db.MS_CITY.Add(mS_CITY);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_ID_PROVENCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE", mS_CITY.FK_ID_PROVENCE);
            return View(mS_CITY);
        }

        

        // GET: City/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_CITY mS_CITY = db.MS_CITY.Find(id);
            db.MS_CITY.Remove(mS_CITY);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
