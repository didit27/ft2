﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class GenderController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Gender
        public ActionResult Index()
        {
            return View(db.MS_GENDER.ToList());
        }

        // GET: Gender/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_GENDER mS_GENDER = db.MS_GENDER.Find(id);
            if (mS_GENDER == null)
            {
                return HttpNotFound();
            }
            return View(mS_GENDER);
        }

        // GET: Gender/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Gender/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_GENDER,GENDER")] MS_GENDER mS_GENDER)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.MS_GENDER.Add(mS_GENDER);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
            }

            return View(mS_GENDER);
        }

        // GET: Gender/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_GENDER mS_GENDER = db.MS_GENDER.Find(id);
            if (mS_GENDER == null)
            {
                return HttpNotFound();
            }
            return View(mS_GENDER);
        }

        // POST: Gender/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_GENDER,GENDER")] MS_GENDER mS_GENDER)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(mS_GENDER).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception )
                    {

                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                } 
            }
            return View(mS_GENDER);
        }

        // GET: Gender/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_GENDER mS_GENDER = db.MS_GENDER.Find(id);
            if (mS_GENDER == null)
            {
                return HttpNotFound();
            }
            return View(mS_GENDER);
        }

        // POST: Gender/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    MS_GENDER mS_GENDER = db.MS_GENDER.Find(id);
                    db.MS_GENDER.Remove(mS_GENDER);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch(Exception )
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();    
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
