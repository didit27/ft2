﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    public class AnnounceController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Announce
        public ActionResult Index()
        {
            return View(db.TBL_ANNOUNCE.ToList());
        }

        // GET: Announce/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_ANNOUNCE tBL_ANNOUNCE = db.TBL_ANNOUNCE.Find(id);
            if (tBL_ANNOUNCE == null)
            {
                return HttpNotFound();
            }
            return View(tBL_ANNOUNCE);
        }

        // GET: Announce/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Announce/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_ANNOUNCE,ANNOUNCEMENT")] TBL_ANNOUNCE tBL_ANNOUNCE)
        {
            if (ModelState.IsValid)
            {
                db.TBL_ANNOUNCE.Add(tBL_ANNOUNCE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tBL_ANNOUNCE);
        }

        // GET: Announce/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_ANNOUNCE tBL_ANNOUNCE = db.TBL_ANNOUNCE.Find(id);
            if (tBL_ANNOUNCE == null)
            {
                return HttpNotFound();
            }
            return View(tBL_ANNOUNCE);
        }

        // POST: Announce/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ANNOUNCE,ANNOUNCEMENT")] TBL_ANNOUNCE tBL_ANNOUNCE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tBL_ANNOUNCE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tBL_ANNOUNCE);
        }

        // GET: Announce/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_ANNOUNCE tBL_ANNOUNCE = db.TBL_ANNOUNCE.Find(id);
            if (tBL_ANNOUNCE == null)
            {
                return HttpNotFound();
            }
            return View(tBL_ANNOUNCE);
        }

        // POST: Announce/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TBL_ANNOUNCE tBL_ANNOUNCE = db.TBL_ANNOUNCE.Find(id);
            db.TBL_ANNOUNCE.Remove(tBL_ANNOUNCE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
