﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using PengRaSi;

namespace PengRaSi.Controllers
{
    public class AttendanceController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Attendance
        public ActionResult Index()
        {
            
            return View();
        }

        // GET: Attendance/Details/5
        

        // GET: Attendance/Create
        public ActionResult Insert(string id, int idAttend)
        {
            var cekAttc = db.TBL_ATTENDANCE.Where(x => x.FK_ID_STUDENT == id && x.FK_ID_CLASS_ATTEND == idAttend).FirstOrDefault();

            if (cekAttc == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["studentname"] = cekAttc.TBL_STUDENT.STUDENT_NAME;
                
            } 
               


            if (cekAttc != null)
            {
                return View(cekAttc);
            }

            TBL_ATTENDANCE tbl_Att = new TBL_ATTENDANCE
            {
                FK_ID_STUDENT = id,
                FK_ID_CLASS_ATTEND = idAttend
            };

            return View(tbl_Att);
        }

        // POST: Attendance/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert([Bind(Include = "ID_ATTEND,FK_ID_STUDENT,ILL,PERMIT,ABSENT,FK_ID_CLASS_ATTEND")] TBL_ATTENDANCE tBL_ATTENDANCE)
        {
            var cekAtt = db.TBL_ATTENDANCE.Where(x => x.FK_ID_STUDENT == tBL_ATTENDANCE.FK_ID_STUDENT && x.FK_ID_CLASS_ATTEND == tBL_ATTENDANCE.FK_ID_CLASS_ATTEND).FirstOrDefault();


            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (cekAtt != null)
                        {
                            db.TBL_ATTENDANCE.Remove(cekAtt);
                            db.SaveChanges();
                            //ts.Commit();
                        }
                        db.TBL_ATTENDANCE.Add(tBL_ATTENDANCE);
                        db.SaveChanges();
                        ts.Commit();

                        return RedirectToAction("IndexHRAttendance", "Student", routeValues: new { id = tBL_ATTENDANCE.FK_ID_CLASS_ATTEND });
                    }
                    catch
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
            }
            return View(tBL_ATTENDANCE);
        }


       
    }
}
