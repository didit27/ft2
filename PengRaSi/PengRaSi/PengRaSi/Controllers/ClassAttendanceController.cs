﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PengRaSi.Controllers
{
    public class ClassAttendanceController : Controller
    {
        public pengrasiEntities DB = new pengrasiEntities();
        // GET: ClassAttendance
        public ActionResult Index()
        {
            var Teach = DB.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).FirstOrDefault();
            var Class = DB.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == Teach.FK_ID_TEACHER).FirstOrDefault();
            var ClassName = DB.TBL_CLASS.Where(x => x.ID_CLASS == Class.FK_ID_CLASS).FirstOrDefault();
            ViewData["ClassName"] = ClassName.CLASS_NAME;
            ViewData["ClassID"] = ClassName.ID_CLASS;
            return View(DB.TBL_CLASS_ATTEND.ToList());
        }

        public ActionResult Create(string id)
        {
            TBL_CLASS_ATTEND tBL_CLASS_ATTEND = new TBL_CLASS_ATTEND
            {
                FK_ID_CLASS = id
            };

            return View(tBL_CLASS_ATTEND);
        }
        [HttpPost]
        public ActionResult Create([Bind(Include = "ID_CLASS_ATTEND, FK_ID_CLASS, SMT")] TBL_CLASS_ATTEND tBL_CLASS_ATTEND)
        {
            DB.TBL_CLASS_ATTEND.Add(tBL_CLASS_ATTEND);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            TBL_CLASS_ATTEND tBL_CLASS_ATTEND = DB.TBL_CLASS_ATTEND.Find(id);
            DB.TBL_CLASS_ATTEND.Remove(tBL_CLASS_ATTEND);
            DB.SaveChanges();
            
            return RedirectToAction("Index");
        }

    }


}