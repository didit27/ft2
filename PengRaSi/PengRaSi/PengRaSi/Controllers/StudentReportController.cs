﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace PengRaSi.Controllers
{
    public class StudentReportController : Controller
    {
        pengrasiEntities DB = new pengrasiEntities();
        // GET: StudentReport
        public ActionResult Index(int id)
        {
            var idTeach = DB.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).First();
            var idClass = DB.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == idTeach).Select(x => x.FK_ID_CLASS).First();
            var tBL_STUDENT = DB.TBL_STUDENT.Where(x => x.FK_ID_CLASS == idClass);
            ViewData["SMT"] = id;

            return View(tBL_STUDENT);
        }
        public ActionResult SMTSel()
        {
            var idTeach = DB.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).First();
            var idClass = DB.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == idTeach).Select(x => x.FK_ID_CLASS).First();
            var smtList = DB.TBL_SUBJECT_CLASS.Where(x => x.FK_ID_CLASS == idClass).AsEnumerable().GroupBy(x=>x.SMT).Select(x=>x.First());
            ViewBag.SMT = new SelectList(smtList, "SMT", "SMT");
            return View();
        }
        [HttpPost, ActionName("SMTSel")]
        public ActionResult SMTPost(int SMT)
        {
            //var SMT = Request["SMT"].ToString();
            return RedirectToAction("Index","StudentReport", new {id = SMT });
        }

        public ActionResult ShowReport(string id, int smt)
        {
            var id_teach = DB.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x=>x.FK_ID_TEACHER).FirstOrDefault();

            ReportViewer Report = new ReportViewer();
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.Width = Unit.Percentage(100);
            Report.BackColor = System.Drawing.Color.WhiteSmoke;
            Report.SplitterBackColor = System.Drawing.Color.LightGray;
            string UrlReportServer = "http://localhost/ReportServer";
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.ServerReport.ReportServerUrl = new Uri(UrlReportServer);
            Report.ServerReport.ReportPath = "/PengrasiStudentReport1";
            Report.ServerReport.Refresh();

            ReportParameter[] parameters = new ReportParameter[3];
            parameters[0] = new ReportParameter("ID", id, true);
            parameters[1] = new ReportParameter("SMT", smt.ToString(), true);
            parameters[2] = new ReportParameter("IDTEAC", id_teach, true);
            Report.ServerReport.SetParameters(parameters);
            Report.ServerReport.Refresh();
            var idStudent = DB.TBL_STUDENT.Where(x => x.ID_STUDENT == id).FirstOrDefault();
            var idClass = DB.TBL_CLASS.Where(x => x.ID_CLASS == idStudent.FK_ID_CLASS).FirstOrDefault();
            Report.ServerReport.DisplayName = "[RAPORT]"+idStudent.STUDENT_NAME+" Kelas"+idClass.CLASS_NAME+" Tahun"+idClass.SCH_YEAR+" Semester"+smt;
            

            ViewBag.ReportViewer = Report;
            return View();
        }
        public ActionResult ShowStudentBio(string id)
        {
            ReportViewer Report = new ReportViewer();
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.Width = Unit.Percentage(100);
            Report.BackColor = System.Drawing.Color.WhiteSmoke;
            Report.SplitterBackColor = System.Drawing.Color.LightGray;
            string UrlReportServer = "http://localhost/ReportServer";
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.ServerReport.ReportServerUrl = new Uri(UrlReportServer);
            Report.ServerReport.ReportPath = "/PengrasiStudentData";
            Report.ServerReport.Refresh();

            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("ID", id, true);
            Report.ServerReport.SetParameters(parameters);
            Report.ServerReport.Refresh();
            var idStudent = DB.TBL_STUDENT.Where(x => x.ID_STUDENT == id).FirstOrDefault();
            var idClass = DB.TBL_CLASS.Where(x => x.ID_CLASS == idStudent.FK_ID_CLASS).FirstOrDefault();
            Report.ServerReport.DisplayName = "[BIODATA]"+idStudent.STUDENT_NAME+" NIS"+idStudent.NIS;

            ViewBag.ReportViewer = Report;
            return View();
        }



        public ActionResult ScoreStudentAdmin(string id)
        {
            ReportViewer Report = new ReportViewer();
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.Width = Unit.Percentage(100);
            Report.BackColor = System.Drawing.Color.WhiteSmoke;
            Report.SplitterBackColor = System.Drawing.Color.LightGray;
            string UrlReportServer = "http://localhost/ReportServer";
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.ServerReport.ReportServerUrl = new Uri(UrlReportServer);
            Report.ServerReport.ReportPath = "/ScoreStudent";
            Report.ServerReport.Refresh();

            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("ID", id, true);
            Report.ServerReport.SetParameters(parameters);
            Report.ServerReport.Refresh();
            var idStudent = DB.TBL_STUDENT.Where(x => x.ID_STUDENT == id).FirstOrDefault();
            var idClass = DB.TBL_CLASS.Where(x => x.ID_CLASS == idStudent.FK_ID_CLASS).FirstOrDefault();
            Report.ServerReport.DisplayName = "[SCORE]" + idStudent.STUDENT_NAME + " NIS" + idStudent.NIS;

            ViewBag.ReportViewer = Report;
            return View();
        }
    }
}