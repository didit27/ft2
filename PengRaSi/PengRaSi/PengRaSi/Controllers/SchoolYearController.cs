﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class SchoolYearController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: SchoolYear
        public ActionResult Index()
        {
            return View(db.MS_SCH_YEAR.ToList());
        }

        // GET: SchoolYear/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_SCH_YEAR mS_SCH_YEAR = db.MS_SCH_YEAR.Find(id);
            if (mS_SCH_YEAR == null)
            {
                return HttpNotFound();
            }
            return View(mS_SCH_YEAR);
        }

        // GET: SchoolYear/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SchoolYear/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_SCH_YEAR,SCH_YEAR")] MS_SCH_YEAR mS_SCH_YEAR)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.MS_SCH_YEAR.Add(mS_SCH_YEAR);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception )
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
            }

            return View(mS_SCH_YEAR);
        }

        // GET: SchoolYear/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_SCH_YEAR mS_SCH_YEAR = db.MS_SCH_YEAR.Find(id);
            if (mS_SCH_YEAR == null)
            {
                return HttpNotFound();
            }
            return View(mS_SCH_YEAR);
        }

        // POST: SchoolYear/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_SCH_YEAR,SCH_YEAR")] MS_SCH_YEAR mS_SCH_YEAR)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(mS_SCH_YEAR).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception )
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }    
            }
            return View(mS_SCH_YEAR);
        }

        // GET: SchoolYear/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_SCH_YEAR mS_SCH_YEAR = db.MS_SCH_YEAR.Find(id);
            if (mS_SCH_YEAR == null)
            {
                return HttpNotFound();
            }
            return View(mS_SCH_YEAR);
        }

        // POST: SchoolYear/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    MS_SCH_YEAR mS_SCH_YEAR = db.MS_SCH_YEAR.Find(id);
                    db.MS_SCH_YEAR.Remove(mS_SCH_YEAR);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception )
                {

                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";

                }
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
