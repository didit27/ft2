﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    public class TrackNotifController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: TrackNotif
        public ActionResult Index()
        {
            return View(db.TBL_TRACKNOTIF.OrderByDescending(x => x.ID_TRACKNOTIF).ToList());
        }
        public ActionResult IndexTeach()
        {
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            return View(db.TBL_TRACKNOTIF.Where(x => x.FK_ID_USER_RECEIVER == tb_user.ID_USER || x.SENDER == tb_user.ID_USER).OrderByDescending(x => x.ID_TRACKNOTIF).ToList());
        }

        // GET: TrackNotif/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_TRACKNOTIF tBL_TRACKNOTIF = db.TBL_TRACKNOTIF.Find(id);
            if (tBL_TRACKNOTIF == null)
            {
                return HttpNotFound();
            }
            return View(tBL_TRACKNOTIF);
        }

        // GET: TrackNotif/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrackNotif/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_TRACKNOTIF,INBOX,FK_ID_USER_RECEIVER,SENDER,DATE_NOTIF,ACTION_NOTIF,DATE_ACTION")] TBL_TRACKNOTIF tBL_TRACKNOTIF)
        {
            if (ModelState.IsValid)
            {
                db.TBL_TRACKNOTIF.Add(tBL_TRACKNOTIF);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tBL_TRACKNOTIF);
        }

        // GET: TrackNotif/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_TRACKNOTIF tBL_TRACKNOTIF = db.TBL_TRACKNOTIF.Find(id);
            if (tBL_TRACKNOTIF == null)
            {
                return HttpNotFound();
            }
            return View(tBL_TRACKNOTIF);
        }

        // POST: TrackNotif/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TRACKNOTIF,INBOX,FK_ID_USER_RECEIVER,SENDER,DATE_NOTIF,ACTION_NOTIF,DATE_ACTION")] TBL_TRACKNOTIF tBL_TRACKNOTIF)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tBL_TRACKNOTIF).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tBL_TRACKNOTIF);
        }

        // GET: TrackNotif/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_TRACKNOTIF tBL_TRACKNOTIF = db.TBL_TRACKNOTIF.Find(id);
            if (tBL_TRACKNOTIF == null)
            {
                return HttpNotFound();
            }
            return View(tBL_TRACKNOTIF);
        }

        // POST: TrackNotif/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TBL_TRACKNOTIF tBL_TRACKNOTIF = db.TBL_TRACKNOTIF.Find(id);
            db.TBL_TRACKNOTIF.Remove(tBL_TRACKNOTIF);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
