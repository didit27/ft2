﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using WebGrease.Css.Extensions;

namespace PengRaSi.Controllers
{
    [Authorize]
    
    public class StudentController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Student
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            var tBL_STUDENT = db.TBL_STUDENT.Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);
            
            return View(tBL_STUDENT.OrderByDescending(x => x.no).ToList());
        }

        // GET: Student/Details/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // GET: Student/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER");
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION");
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME");
            
            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE");

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create([Bind(Include = "STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {
           
            string a = tBL_STUDENT.ID_STUDENT;
            string b = tBL_STUDENT.NIS;
            string c = tBL_STUDENT.FK_ID_CLASS;
            TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            TBL_CLASS tbl_studentyear = db.TBL_CLASS.Where(x => x.ID_CLASS == c).FirstOrDefault();

            var tbvalidasi = db.TBL_STUDENT.Where(x => x.NIS == b && x.TBL_CLASS.SCH_YEAR==tbl_studentyear.SCH_YEAR).FirstOrDefault();
       
          


            if (tbl_studentvalidasiid == null)
            {
                if (tbvalidasi!=null)
                {
                    ViewData["nissame"] = "NIS Same";
                   
                }
                else
                {
                    if(tBL_STUDENT.NIS.Length >= 8 && tBL_STUDENT.NIS.Length <= 12)
                    {
                        using (DbContextTransaction ts = db.Database.BeginTransaction())
                        {
                            try
                            {
                                tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                                db.TBL_STUDENT.Add(tBL_STUDENT);
                                db.SaveChanges();
                                ts.Commit();
                                return RedirectToAction("Index");
                            }
                            catch
                            {
                                ts.Rollback();
                                ViewData["data"] = "Something Error Please Try Again";
                            }
                        }
                    }
                    else
                    {
                        ViewData["data"] = "NIS minimum length is 8 and maximum is 12";
                    }
                    
                    
                }

            }
            else
            {
                ViewData["idstudentsama"] = "Id Student Same";
            }
            //if (ModelState.IsValid)
            //{
            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0,2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length-4),4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);
            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE");
            return View(tBL_STUDENT);
        }

        // GET: Student/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }

            
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);

            //dropdown province and city
            var cityId = db.TBL_STUDENT.Where(x => x.ID_STUDENT == id).Select(x => x.BIRTHPLACE).FirstOrDefault();
            var getProvId = db.MS_CITY.Where(x => x.ID_CITY == cityId).Select(x => x.FK_ID_PROVENCE).FirstOrDefault();
            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE",getProvId);
            List<MS_CITY> cityList = db.MS_CITY.ToList();
            ViewBag.City = new SelectList(cityList,"ID_CITY","CITY");

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View(tBL_STUDENT);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "ID_STUDENT,STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {
            ////string a = tBL_STUDENT.ID_STUDENT;
            ////string b = tBL_STUDENT.NIS;
            ////TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis2 = db.TBL_STUDENT.Where(x => x.NIS == tbl_studentvalidasiid.NIS).FirstOrDefault();
            ////string d = tbl_studentvalidasinis2.NIS;
            ////string c = tBL_STUDENT.NIS;
            ///
            if (tBL_STUDENT.NIS.Length >= 8 && tBL_STUDENT.NIS.Length <= 12)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {

                        ////TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
                        ////db.TBL_STUDENT.Remove(tBL1_STUDENT);
                        ////db.SaveChanges();


                        ////if (tbl_studentvalidasinis == null || c == d)
                        ////{
                        ////    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                        ////    db.TBL_STUDENT.Add(tBL_STUDENT);
                        ////    db.SaveChanges();
                        ////    ts.Commit();
                        ////    return RedirectToAction("Index");
                        ////}
                        ////else
                        ////{
                        ////    ViewData["data"] = "Something Error Or NIS Same";
                        ////}

                        db.Entry(tBL_STUDENT).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Or NIS Same";
                    }
                }
            }
            else
            {
                ViewData["data"] = "NIS minimum length is 8 and maximum is 12";
            }
                

            //if (ModelState.IsValid)
            //{
            //    TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
            //    db.TBL_STUDENT.Remove(tBL1_STUDENT);
            //    db.SaveChanges();

            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    //db.Entry(tBL_STUDENT).State = EntityState.Modified;
            //    //db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);

            //dropdown province and city
            var cityId = db.TBL_STUDENT.Where(x => x.ID_STUDENT == tBL_STUDENT.ID_STUDENT).Select(x => x.BIRTHPLACE).FirstOrDefault();
            var getProvId = db.MS_CITY.Where(x => x.ID_CITY == cityId).Select(x => x.FK_ID_PROVENCE).FirstOrDefault();
            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE", getProvId);
            List<MS_CITY> cityList = db.MS_CITY.ToList();
            ViewBag.City = new SelectList(cityList, "ID_CITY", "CITY");
            return View(tBL_STUDENT);
        }

        // GET: Student/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
                    db.TBL_STUDENT.Remove(tBL_STUDENT);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error ";
                }
            }
            return View();
        }

        //----------------------------HOOMROOM TEACHER-------------------)
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult IndexHT()
        {

            var a = System.Web.HttpContext.Current.User.Identity.Name;
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_user.FK_ID_TEACHER).FirstOrDefault();
            TBL_HOMEROOM tb_homeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tb_teacher.ID_TEACHER).FirstOrDefault();
            TBL_CLASS tb_class = db.TBL_CLASS.Where(x => x.ID_CLASS == tb_homeroom.FK_ID_CLASS).FirstOrDefault();
            TBL_STUDENT tb_student = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tb_class.ID_CLASS).FirstOrDefault();

            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tb_student.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);

            return View(tBL_STUDENT.OrderByDescending(x => x.no).ToList());
        }

        // GET: Student/Details/5
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult DetailsHT(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // GET: Student/Create
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult CreateHT()
        {
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER");
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION");
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME");

            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE");

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult CreateHT([Bind(Include = "STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {

            //string a = tBL_STUDENT.ID_STUDENT;
            //string b = tBL_STUDENT.NIS;
            //TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            //TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();


            ////var a = System.Web.HttpContext.Current.User.Identity.Name;
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_user.FK_ID_TEACHER).FirstOrDefault();
            TBL_HOMEROOM tb_homeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tb_teacher.ID_TEACHER).FirstOrDefault();
            TBL_CLASS tb_class = db.TBL_CLASS.Where(x => x.ID_CLASS == tb_homeroom.FK_ID_CLASS).FirstOrDefault();
            //TBL_STUDENT tb_student = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tb_class.ID_CLASS).FirstOrDefault();
            tBL_STUDENT.FK_ID_CLASS = tb_class.ID_CLASS;
            string a = tBL_STUDENT.ID_STUDENT;
            string b = tBL_STUDENT.NIS;
            string c = tBL_STUDENT.FK_ID_CLASS;
            TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            TBL_CLASS tbl_studentyear = db.TBL_CLASS.Where(x => x.ID_CLASS == c).FirstOrDefault();

            var tbvalidasi = db.TBL_STUDENT.Where(x => x.NIS == b && x.TBL_CLASS.SCH_YEAR == tbl_studentyear.SCH_YEAR).FirstOrDefault();


            if (tbl_studentvalidasiid == null)
            {
                if (tbvalidasi != null)
                {
                    ViewData["nissame"] = "NIS Same";

                }
                else
                {
                    if (tBL_STUDENT.NIS.Length >= 8 && tBL_STUDENT.NIS.Length <= 12)
                    {
                        using (DbContextTransaction ts = db.Database.BeginTransaction())
                        {
                            try
                            {
                                tBL_STUDENT.FK_ID_CLASS = tb_class.ID_CLASS;
                                tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                                db.TBL_STUDENT.Add(tBL_STUDENT);
                                db.SaveChanges();
                                ts.Commit();
                                return RedirectToAction("IndexHT");
                            }
                            catch
                            {
                                ts.Rollback();
                                ViewData["data"] = "Something Error Please Try Again";
                            }
                        }
                    }
                    else
                    {
                        ViewData["data"] = "NIS minimum length is 8 and maximum is 12";
                    }
                        

                }

            }
            else
            {
                ViewData["idstudentsama"] = "Id Student Same";
            }


            //if (tbl_studentvalidasiid == null)
            //{
            //    if (tbl_studentvalidasinis == null)
            //    {
            //        using (DbContextTransaction ts = db.Database.BeginTransaction())
            //        {
            //            try
            //            {
            //                tBL_STUDENT.FK_ID_CLASS = tb_class.ID_CLASS;
            //                tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                            
            //                db.TBL_STUDENT.Add(tBL_STUDENT);
            //                db.SaveChanges();
            //                ts.Commit();
            //                return RedirectToAction("IndexHT");
            //            }
            //            catch
            //            {
            //                ts.Rollback();
            //                ViewData["data"] = "Something Error Please Try Again";
            //            }
            //        }
            //    }
            //    else
            //    {
            //        ViewData["nissame"] = "NIS Same";
            //    }

            //}
            //else
            //{
            //    ViewData["idstudentsama"] = "Id Student Same";
            //}
            //if (ModelState.IsValid)
            //{
            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0,2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length-4),4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);
            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE");
            return View(tBL_STUDENT);
        }

        // GET: Student/Edit/5
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult EditHT(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);

            //dropdown province and city
            var cityId = db.TBL_STUDENT.Where(x => x.ID_STUDENT == id).Select(x => x.BIRTHPLACE).FirstOrDefault();
            var getProvId = db.MS_CITY.Where(x => x.ID_CITY == cityId).Select(x => x.FK_ID_PROVENCE).FirstOrDefault();
            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE", getProvId);
            List<MS_CITY> cityList = db.MS_CITY.ToList();
            ViewBag.City = new SelectList(cityList, "ID_CITY", "CITY");

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View(tBL_STUDENT);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult EditHT([Bind(Include = "ID_STUDENT,STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {
            ////string a = tBL_STUDENT.ID_STUDENT;
            ////string b = tBL_STUDENT.NIS;
            ////TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis2 = db.TBL_STUDENT.Where(x => x.NIS == tbl_studentvalidasiid.NIS).FirstOrDefault();
            ////string d = tbl_studentvalidasinis2.NIS;
            ////string c = tBL_STUDENT.NIS;
            ///
            if (tBL_STUDENT.NIS.Length >= 8 && tBL_STUDENT.NIS.Length <= 12)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {

                        ////TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
                        ////db.TBL_STUDENT.Remove(tBL1_STUDENT);
                        ////db.SaveChanges();


                        ////if (tbl_studentvalidasinis == null || c == d)
                        ////{
                        ////    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                        ////    db.TBL_STUDENT.Add(tBL_STUDENT);
                        ////    db.SaveChanges();
                        ////    ts.Commit();
                        ////    return RedirectToAction("IndexHT");
                        ////}
                        ////else
                        ////{
                        ////    ViewData["data"] = "Something Error Or NIS Same";
                        ////}


                        db.Entry(tBL_STUDENT).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("IndexHT");

                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Or NIS Same";
                    }
                }
            }
            else
            {
                ViewData["data"] = "NIS minimum length is 8 and maximum is 12";
            }
                

            //if (ModelState.IsValid)
            //{
            //    TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
            //    db.TBL_STUDENT.Remove(tBL1_STUDENT);
            //    db.SaveChanges();

            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    //db.Entry(tBL_STUDENT).State = EntityState.Modified;
            //    //db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);
            
            //dropdown province and city
            var cityId = db.TBL_STUDENT.Where(x => x.ID_STUDENT == tBL_STUDENT.ID_STUDENT).Select(x => x.BIRTHPLACE).FirstOrDefault();
            var getProvId = db.MS_CITY.Where(x => x.ID_CITY == cityId).Select(x => x.FK_ID_PROVENCE).FirstOrDefault();
            ViewBag.PROVINCE = new SelectList(db.MS_PROV, "ID_PROV", "PROVINCE", getProvId);
            List<MS_CITY> cityList = db.MS_CITY.ToList();
            ViewBag.City = new SelectList(cityList, "ID_CITY", "CITY");
            return View(tBL_STUDENT);
        }

        // GET: Student/Delete/5
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult DeleteHT(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("DeleteHT")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult DeleteConfirmedHT(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
                    db.TBL_STUDENT.Remove(tBL_STUDENT);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("IndexHT");
                }
                catch
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error ";
                }
            }
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult IndexTeacher(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            
            if(tBL_SUBJECT_CLASS == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
                var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

                var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


                var tb_Student = db.StudentScores.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

                ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
                ViewData["Class"] = tBL_CLASS.CLASS_NAME;
                ViewData["SubjClass"] = id;
                return View(tb_Student);
            }
            

           
        }


        public ActionResult IndexTeacherScoreAtitude(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();

            if (tBL_SUBJECT_CLASS == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
                var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

                var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


                var tb_Student = db.StudentAttitudes.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

                ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
                ViewData["Class"] = tBL_CLASS.CLASS_NAME;
                ViewData["SubjClass"] = id;

                return View(tb_Student);
            }

                
        }

        public ActionResult IndexTeacherScorePractices(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            if (tBL_SUBJECT_CLASS == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
                var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

                var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


                var tb_Student = db.StudentPractices.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

                ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
                ViewData["Class"] = tBL_CLASS.CLASS_NAME;
                ViewData["SubjClass"] = id;

                return View(tb_Student);
            }

               
        }

        public ActionResult IndexHRScoreTheory(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            if (tBL_SUBJECT_CLASS == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
                var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

                var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


                var tb_Student = db.StudentScores.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

                ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
                ViewData["Class"] = tBL_CLASS.CLASS_NAME;
                ViewData["SubjClass"] = id;

                return View(tb_Student);
            }
                
        }

        public ActionResult IndexHRScoreAtitude(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            if (tBL_SUBJECT_CLASS == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
                var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

                var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


                var tb_Student = db.StudentAttitudes.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

                ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
                ViewData["Class"] = tBL_CLASS.CLASS_NAME;
                ViewData["SubjClass"] = id;

                return View(tb_Student);
            }
               
        }
        public ActionResult IndexHRScorePractices(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            if (tBL_SUBJECT_CLASS == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
                var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

                var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


                var tb_Student = db.StudentPractices.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

                ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
                ViewData["Class"] = tBL_CLASS.CLASS_NAME;
                ViewData["SubjClass"] = id;

                return View(tb_Student);
            }
               
        }


        public ActionResult IndexHRAttendance(int id)
        {
            
            var tb_Student = db.StudentAttendances.Where(x => x.ID_CLASS_ATTEND == id );

            var idAttClass = db.TBL_CLASS_ATTEND.Where(x => x.ID_CLASS_ATTEND == id).FirstOrDefault();
            //ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
            //ViewData["Class"] = tBL_CLASS.CLASS_NAME;
            
            if(tb_Student == null || idAttClass == null)
            {
                return RedirectToAction("Index", "NotFound404");
            }
            else
            {
                ViewData["IdClassAtt"] = id;
                ViewData["IdClass"] = idAttClass.FK_ID_CLASS;
            }
         

            return View(tb_Student);
        }


        public ActionResult ShowReportAttendanceStudent(int idClassAtt, string idClass)
        {
            ReportViewer Report = new ReportViewer();
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.Width = Unit.Percentage(100);
            Report.BackColor = System.Drawing.Color.WhiteSmoke;
            Report.SplitterBackColor = System.Drawing.Color.LightGray;
            string UrlReportServer = "http://localhost/ReportServer";
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.ServerReport.ReportServerUrl = new Uri(UrlReportServer);
            Report.ServerReport.ReportPath = "/ReportStudentAttendance";
            Report.ServerReport.Refresh();

            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("IDCLASSATT", Convert.ToString(idClassAtt), true);
            parameters[1] = new ReportParameter("IDCLASS", idClass, true);
            Report.ServerReport.SetParameters(parameters);
            Report.ServerReport.Refresh();
            //var idStudent = DB.TBL_STUDENT.Where(x => x.ID_STUDENT == id).FirstOrDefault();
            var idClass1 = db.TBL_CLASS.Where(x => x.ID_CLASS == idClass).FirstOrDefault();
            var idHoomroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_CLASS == idClass1.ID_CLASS).FirstOrDefault();
            var idTeacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == idHoomroom.FK_ID_TEACHER).FirstOrDefault();
            var idTableclassattend = db.TBL_CLASS_ATTEND.Where(x => x.FK_ID_CLASS == idClass1.ID_CLASS).FirstOrDefault();
            Report.ServerReport.DisplayName = "[ATTENDANCE STUDENT]" +"Homeroom"+ idTeacher.TEACHER_NAME + " Kelas" + idClass1.CLASS_NAME+ " Tahun"+ idClass1.SCH_YEAR+" Semester"+idTableclassattend.SMT;

            ViewBag.ReportViewer = Report;
            return View();
        }
        //-----------------------------STUDENT REPORTING---------------------------//

        public ActionResult SMTSel()
        {
            var idTeach = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).First();
            var idClass = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == idTeach).Select(x => x.FK_ID_CLASS).First();
            var smtList = db.TBL_SUBJECT_CLASS.Where(x => x.FK_ID_CLASS == idClass).AsEnumerable().GroupBy(x => x.SMT).Select(x => x.First());
            ViewBag.SMT = new SelectList(smtList, "SMT", "SMT");
            return View();
        }
        [HttpPost, ActionName("SMTSel")]
        public ActionResult SMTPost(int SMT)
        {
            //var SMT = Request["SMT"].ToString();
            return RedirectToAction("IndexRankingStudent", "Student", new { id = SMT });
        }

        public ActionResult IndexRankingStudent(int id)
        {
            var idTeach = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).First();
            var idClass = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == idTeach).Select(x => x.FK_ID_CLASS).First();
            var idHMRank = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == idTeach).FirstOrDefault();
            var idClassRank = db.TBL_CLASS.Where(x => x.ID_CLASS == idHMRank.FK_ID_CLASS).FirstOrDefault();

            var StudentRank = db.StudentRanks.Where(x => x.SMT == id && x.SCH_YEAR == idClassRank.SCH_YEAR && x.CLASS_NAME == idClassRank.CLASS_NAME);
            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == idClass);
            ViewData["SMT"] = id;
            ViewData["SCH"] = idClassRank.SCH_YEAR;
            ViewData["CLASS"] = idClassRank.CLASS_NAME;
            return View(StudentRank);
        }


        public ActionResult ShowReportRanking(int sch, int smt,string clas)
        {
            var id_teach = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).Select(x => x.FK_ID_TEACHER).FirstOrDefault();

            ReportViewer Report = new ReportViewer();
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.Width = Unit.Percentage(100);
            Report.BackColor = System.Drawing.Color.WhiteSmoke;
            Report.SplitterBackColor = System.Drawing.Color.LightGray;
            string UrlReportServer = "http://localhost/ReportServer";
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.ServerReport.ReportServerUrl = new Uri(UrlReportServer);
            Report.ServerReport.ReportPath = "/StudentRanking";
            Report.ServerReport.Refresh();

            ReportParameter[] parameters = new ReportParameter[4];
            parameters[0] = new ReportParameter("SCH", sch.ToString(), true);
            parameters[1] = new ReportParameter("SMT", smt.ToString(), true);
            parameters[2] = new ReportParameter("CLASS", clas, true);
            parameters[3] = new ReportParameter("IDTEACH", id_teach, true);
            Report.ServerReport.SetParameters(parameters);
            Report.ServerReport.Refresh();
            var idTeach = db.TBL_TEACHER.Where(x => x.ID_TEACHER == id_teach).FirstOrDefault();
            Report.ServerReport.DisplayName = "[RANKING]"+idTeach.TEACHER_NAME+"Kelas"+clas+"Tahun"+sch+"Semester"+smt;


            ViewBag.ReportViewer = Report;
            return View();
        }

        //--------------------------------------------------------------------------------------//
        //fungsi panggil data kota dari pilihan provinsi
       public JsonResult getCity(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<MS_CITY> cityList = db.MS_CITY.Where(x => x.FK_ID_PROVENCE == id).ToList();
            return Json(cityList, JsonRequestBehavior.AllowGet);
        }
    }
}
