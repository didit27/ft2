﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PengRaSi.Controllers
{
    public class SubjectHomeroomController : Controller
    {
        pengrasiEntities db = new pengrasiEntities();
        // GET: SubjectHomeroom
        public ActionResult Index()
        {
            var tbGetTeachId = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).FirstOrDefault();
            var tbGetTeacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tbGetTeachId.FK_ID_TEACHER).FirstOrDefault();
            var tbHomeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tbGetTeachId.FK_ID_TEACHER).FirstOrDefault();
            var tbSubject = db.TBL_SUBJECT_HOMEROOM.Where(x => x.TBL_SUBJECT_CLASS.FK_ID_CLASS == tbHomeroom.FK_ID_CLASS);
            ViewData["Teacher"] = tbGetTeacher.TEACHER_NAME;

            return View(tbSubject);
        }
        
        public ActionResult GrantAcc(int id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    var idSubjClass = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).Select(x => x.ID_SUBJECT_CLASS);
                    TBL_SUBJECT_HOMEROOM tBL_SUBJECT_HOMEROOM = new TBL_SUBJECT_HOMEROOM();
                    tBL_SUBJECT_HOMEROOM.FK_ID_SUBJECT_CLASS = idSubjClass.FirstOrDefault();
                    db.TBL_SUBJECT_HOMEROOM.Add(tBL_SUBJECT_HOMEROOM);

                    var idSubjClassNotif = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
                    var idClassNotif = db.TBL_CLASS.Where(x => x.ID_CLASS == idSubjClassNotif.FK_ID_CLASS).FirstOrDefault();
                    var idHoomroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_CLASS == idClassNotif.ID_CLASS).FirstOrDefault();
                    var idTeacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == idHoomroom.FK_ID_TEACHER).FirstOrDefault();
                    var idUser = db.TBL_USER.Where(x => x.FK_ID_TEACHER == idTeacher.ID_TEACHER).FirstOrDefault();

                    TBL_NOTIF tBL_NOTIF = new TBL_NOTIF();
                    tBL_NOTIF.INBOX = "You Have Been Granted Acces To Student Score "+idSubjClassNotif.TBL_SUBJECT.SUBJECT_NAME;
                    tBL_NOTIF.FK_ID_USER_RECEIVER = idUser.ID_USER;
                    tBL_NOTIF.SENDER = idSubjClassNotif.TBL_TEACHER.TEACHER_NAME;
                    tBL_NOTIF.DATE_NOTIF = DateTime.Now;

                    TBL_TRACKNOTIF tBL_TRACKNOTIF = new TBL_TRACKNOTIF();
                    tBL_TRACKNOTIF.INBOX = tBL_NOTIF.INBOX;
                    tBL_TRACKNOTIF.FK_ID_USER_RECEIVER = tBL_NOTIF.FK_ID_USER_RECEIVER;
                    tBL_TRACKNOTIF.SENDER = tBL_NOTIF.SENDER;
                    tBL_TRACKNOTIF.DATE_NOTIF = tBL_NOTIF.DATE_NOTIF;
                    tBL_TRACKNOTIF.DATE_ACTION = DateTime.Now;
                    TBL_USER tb_userNOTIF = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
                    TBL_TEACHER tb_teacherNOTIF = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_userNOTIF.FK_ID_TEACHER).FirstOrDefault();
                    tBL_TRACKNOTIF.ACTION_NOTIF = "[GRANT ACCESS MANAGE SCORE] By " + tb_teacherNOTIF.TEACHER_NAME;
                    db.TBL_TRACKNOTIF.Add(tBL_TRACKNOTIF);


                    db.TBL_NOTIF.Add(tBL_NOTIF);
                    db.SaveChanges();

                    ts.Commit();
                    return RedirectToAction("IndexTeach", "ClassSubject");
                }
                catch
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();
        }

        public ActionResult DenyAcc(int id)
        {

            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_SUBJECT_HOMEROOM tBL_SUBJECT_HOMEROOM = db.TBL_SUBJECT_HOMEROOM.Find(id);

                    db.TBL_SUBJECT_HOMEROOM.Remove(tBL_SUBJECT_HOMEROOM);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("IndexTeach", "ClassSubject");
                }
                catch
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();
        }
    }

    
}