﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
//using System.Transactions;
using System.Web.Mvc;
using PengRaSi;


namespace PengRaSi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class TeacherController : Controller
    {
        
         pengrasiEntities db = new pengrasiEntities();
       

        // GET: Teacher
        public ActionResult Index()
        {
            var tBL_TEACHER = db.TBL_TEACHER.Include(t => t.MS_GENDER);
            return View(tBL_TEACHER.ToList());
        }

        // GET: Teacher/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_TEACHER tBL_TEACHER = db.TBL_TEACHER.Find(id);
            if (tBL_TEACHER == null)
            {
                return HttpNotFound();
            }
            return View(tBL_TEACHER);
        }

        // GET: Teacher/Create
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.FK_ID_GENDER = new SelectList(db.MS_GENDER,"ID_GENDER","GENDER");
            int i = 1;
            string idGen="";
            while(i>0)
            {
                idGen = "TS" + i.ToString("000");
                var cekID = db.TBL_TEACHER.Find(idGen);
                if(cekID == null)
                {
                    i = -1;
                }
                i++;
            }
            TBL_TEACHER tBL_TEACHER = new TBL_TEACHER
            {
                ID_TEACHER = idGen
            };

            return View(tBL_TEACHER);
        }

        // POST: Teacher/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        //[ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create( TBL_TEACHER tBL_TEACHER)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {

                        db.TBL_TEACHER.Add(tBL_TEACHER);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                        

                    }
                    catch (Exception )
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Or ID Teacher Same";
                    }
                }
            }
            ViewBag.FK_ID_GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER");
            return View();

            /*if (ModelState.IsValid)
           {
               db.TBL_TEACHER.Add(tBL_TEACHER);
               db.SaveChanges();
               return RedirectToAction("Index");
           }*/


        }

        // GET: Teacher/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_TEACHER tBL_TEACHER = db.TBL_TEACHER.Find(id);
            if (tBL_TEACHER == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_ID_GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER",tBL_TEACHER.FK_ID_GENDER);
            return View(tBL_TEACHER);
        }

        // POST: Teacher/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TEACHER,TEACHER_NAME,NIP,FK_ID_GENDER,ADDRESS,TELP")] TBL_TEACHER tBL_TEACHER)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(tBL_TEACHER).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch(Exception )
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
             }
          /*if (ModelState.IsValid)
            {
                db.Entry(tBL_TEACHER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }*/
            return View(tBL_TEACHER);
        }

        // GET: Teacher/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_TEACHER tBL_TEACHER = db.TBL_TEACHER.Find(id);
            if (tBL_TEACHER == null)
            {
                return HttpNotFound();
            }
            return View(tBL_TEACHER);
        }

        // POST: Teacher/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {

                    TBL_TEACHER tBL_TEACHER = db.TBL_TEACHER.Find(id);
                    db.TBL_TEACHER.Remove(tBL_TEACHER);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch(Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
              }
            return View();
        }

        /*protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/
    }
}
