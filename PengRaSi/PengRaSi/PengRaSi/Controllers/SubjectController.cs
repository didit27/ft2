﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    public class SubjectController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Subject
        public ActionResult Index()
        {
            return View(db.TBL_SUBJECT.ToList());
        }

        // GET: Subject/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SUBJECT tBL_SUBJECT = db.TBL_SUBJECT.Find(id);
            if (tBL_SUBJECT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SUBJECT);
        }

        // GET: Subject/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subject/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_SUBJECT,SUBJECT_NAME,SKS")] TBL_SUBJECT tBL_SUBJECT)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.TBL_SUBJECT.Add(tBL_SUBJECT);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
            }

            return View(tBL_SUBJECT);
        }

        // GET: Subject/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SUBJECT tBL_SUBJECT = db.TBL_SUBJECT.Find(id);
            if (tBL_SUBJECT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SUBJECT);
        }

        // POST: Subject/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_SUBJECT,SUBJECT_NAME,SKS")] TBL_SUBJECT tBL_SUBJECT)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(tBL_SUBJECT).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }    
            }
            return View(tBL_SUBJECT);
        }

        // GET: Subject/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SUBJECT tBL_SUBJECT = db.TBL_SUBJECT.Find(id);
            if (tBL_SUBJECT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SUBJECT);
        }

        // POST: Subject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_SUBJECT tBL_SUBJECT = db.TBL_SUBJECT.Find(id);
                    db.TBL_SUBJECT.Remove(tBL_SUBJECT);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
