﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        Models.Encryption code = new Models.Encryption();
        

        // GET: User
        public ActionResult Index()
        {
            var tBL_USER = db.TBL_USER.Include(t => t.MS_ROLE).Include(t => t.TBL_TEACHER);
            return View(tBL_USER.ToList());
        }

        // GET: User/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_USER tBL_USER = db.TBL_USER.Find(id);
            if (tBL_USER == null)
            {
                return HttpNotFound();
            }
            return View(tBL_USER);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            ViewBag.POSITION = new SelectList(db.MS_ROLE, "ROLE", "ROLE");
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME");
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_USER,PASSWORD,POSITION,FK_ID_TEACHER")] TBL_USER tBL_USER)
        {
            string a = tBL_USER.FK_ID_TEACHER;
            string b = tBL_USER.ID_USER;

            //TBL_TEACHER tBL_TEACHERnew = new TBL_TEACHER();

            TBL_USER tb_user = db.TBL_USER.Where(x => x.FK_ID_TEACHER == a).FirstOrDefault();
            TBL_USER tb_user1 = db.TBL_USER.Where(x => x.ID_USER == b).FirstOrDefault();
            TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == a).FirstOrDefault();
            TBL_HOMEROOM tb_homeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tb_teacher.ID_TEACHER).FirstOrDefault();


            TBL_USER tb_userNOTIF = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            TBL_TEACHER tb_teacherNOTIF = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_userNOTIF.FK_ID_TEACHER).FirstOrDefault();

            if (tBL_USER.POSITION == "Homeroom Teacher")
            {
                if (tb_user1 == null)
                {
                    if (tb_user == null)
                    {
                        if (tb_homeroom != null)
                        {
                            using (DbContextTransaction ts = db.Database.BeginTransaction())
                            {
                                try
                                {
                                    String encrypted = code.Encrypt(tBL_USER.PASSWORD);
                                    tBL_USER.PASSWORD = encrypted;
                                    db.TBL_USER.Add(tBL_USER);
                                    //db.SaveChanges();
                                    int valuenotif1 = 0;
                                    if (valuenotif1 == 0)
                                    {
                                        TBL_NOTIF tBL_NOTIF = new TBL_NOTIF();
                                        tBL_NOTIF.INBOX = "Please Changes Your Password";
                                        tBL_NOTIF.FK_ID_USER_RECEIVER = tBL_USER.ID_USER;
                                        tBL_NOTIF.SENDER = tb_teacherNOTIF.TEACHER_NAME;
                                        tBL_NOTIF.DATE_NOTIF = DateTime.Now;
                                        db.TBL_NOTIF.Add(tBL_NOTIF);

                                        TBL_TRACKNOTIF tBL_TRACKNOTIF = new TBL_TRACKNOTIF();
                                        tBL_TRACKNOTIF.INBOX = tBL_NOTIF.INBOX;
                                        tBL_TRACKNOTIF.FK_ID_USER_RECEIVER = tBL_NOTIF.FK_ID_USER_RECEIVER;
                                        tBL_TRACKNOTIF.SENDER = tBL_NOTIF.SENDER;
                                        tBL_TRACKNOTIF.DATE_NOTIF = tBL_NOTIF.DATE_NOTIF;
                                        tBL_TRACKNOTIF.DATE_ACTION = DateTime.Now;
                                        TBL_USER tb_userNOTIF1 = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
                                        TBL_TEACHER tb_teacherNOTIF1 = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_userNOTIF.FK_ID_TEACHER).FirstOrDefault();
                                        tBL_TRACKNOTIF.ACTION_NOTIF = "[CREATE USER] By " + tb_teacherNOTIF1.TEACHER_NAME;
                                        db.TBL_TRACKNOTIF.Add(tBL_TRACKNOTIF);

                                        db.SaveChanges();
                                        ts.Commit();
                                    }
                                    return RedirectToAction("Index");

                                }
                                catch (Exception)
                                {
                                    ts.Rollback();
                                    ViewData["data"] = "Something Error Or Username Same";
                                }
                            }

                        }
                        else
                        {
                            ViewData["homeroom"] = "Please First Add A Teacher Who Wants To Be Homeroom Teacher At Menu Manage Homeroom Teacher";
                        }

                    }
                    else
                    {
                        ViewData["sama"] = "User Already or Username Same..!!";
                    }

                }
                else
                {
                    ViewData["sama"] = "User Already or Username Same..!!";
                }
            }
            else
            {
                if (tb_user == null)
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            String encrypted = code.Encrypt(tBL_USER.PASSWORD);
                            tBL_USER.PASSWORD = encrypted;
                            db.TBL_USER.Add(tBL_USER);
                            //db.SaveChanges();
                            int valuenotif1 = 0;
                            if (valuenotif1 == 0)
                            {
                                TBL_NOTIF tBL_NOTIF = new TBL_NOTIF();
                                tBL_NOTIF.INBOX = "Please Changes Your Password";
                                tBL_NOTIF.FK_ID_USER_RECEIVER = tBL_USER.ID_USER;
                                tBL_NOTIF.SENDER = tb_teacherNOTIF.TEACHER_NAME;
                                tBL_NOTIF.DATE_NOTIF = DateTime.Now;
                                db.TBL_NOTIF.Add(tBL_NOTIF);

                                TBL_TRACKNOTIF tBL_TRACKNOTIF = new TBL_TRACKNOTIF();
                                tBL_TRACKNOTIF.INBOX = tBL_NOTIF.INBOX;
                                tBL_TRACKNOTIF.FK_ID_USER_RECEIVER = tBL_NOTIF.FK_ID_USER_RECEIVER;
                                tBL_TRACKNOTIF.SENDER = tBL_NOTIF.SENDER;
                                tBL_TRACKNOTIF.DATE_NOTIF = tBL_NOTIF.DATE_NOTIF;
                                tBL_TRACKNOTIF.DATE_ACTION = DateTime.Now;
                                TBL_USER tb_userNOTIF1 = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
                                TBL_TEACHER tb_teacherNOTIF1 = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_userNOTIF.FK_ID_TEACHER).FirstOrDefault();
                                tBL_TRACKNOTIF.ACTION_NOTIF = "[CREATE USER] By " + tb_teacherNOTIF1.TEACHER_NAME;
                                db.TBL_TRACKNOTIF.Add(tBL_TRACKNOTIF);
                                db.SaveChanges();
                                ts.Commit();
                            }

                            return RedirectToAction("Index");

                        }
                        catch (Exception)
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Or Username Same";
                        }
                    }
                }
                else
                {
                    ViewData["sama"] = "User Already or Username Same..!!";
                }
                   
            }


            ViewBag.POSITION = new SelectList(db.MS_ROLE, "ROLE", "ROLE", tBL_USER.POSITION);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_USER.FK_ID_TEACHER);
            return View(tBL_USER);
        }

        // GET: User/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_USER tBL_USER = db.TBL_USER.Find(id);
            if (tBL_USER == null)
            {
                return HttpNotFound();
            }
            ViewBag.POSITION = new SelectList(db.MS_ROLE, "ROLE", "ROLE", tBL_USER.POSITION);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_USER.FK_ID_TEACHER);
            return View(tBL_USER);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_USER,PASSWORD,POSITION,FK_ID_TEACHER")] TBL_USER tBL_USER)
        {
            TBL_USER tb_userNOTIF = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            TBL_TEACHER tb_teacherNOTIF = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_userNOTIF.FK_ID_TEACHER).FirstOrDefault();

            string a = tBL_USER.FK_ID_TEACHER;
           
            TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == a).FirstOrDefault();
            TBL_HOMEROOM tb_homeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tb_teacher.ID_TEACHER).FirstOrDefault();



            if (ModelState.IsValid)
            {
                if (tBL_USER.POSITION == "Homeroom Teacher")
                {
                    if(tb_homeroom != null)
                    {
                        using (DbContextTransaction ts = db.Database.BeginTransaction())
                        {
                            try
                            {

                                String encrypted = code.Encrypt(tBL_USER.PASSWORD);
                                tBL_USER.PASSWORD = encrypted;
                                db.Entry(tBL_USER).State = EntityState.Modified;
                                db.SaveChanges();

                                int valuenotif1 = 0;
                                if (valuenotif1 == 0)
                                {
                                    TBL_NOTIF tBL_NOTIF = new TBL_NOTIF();
                                    tBL_NOTIF.INBOX = "Please Changes Your Password";
                                    tBL_NOTIF.FK_ID_USER_RECEIVER = tBL_USER.ID_USER;
                                    tBL_NOTIF.SENDER = tb_teacherNOTIF.TEACHER_NAME;
                                    tBL_NOTIF.DATE_NOTIF = DateTime.Now;
                                    db.TBL_NOTIF.Add(tBL_NOTIF);

                                    TBL_TRACKNOTIF tBL_TRACKNOTIF = new TBL_TRACKNOTIF();
                                    tBL_TRACKNOTIF.INBOX = tBL_NOTIF.INBOX;
                                    tBL_TRACKNOTIF.FK_ID_USER_RECEIVER = tBL_NOTIF.FK_ID_USER_RECEIVER;
                                    tBL_TRACKNOTIF.SENDER = tBL_NOTIF.SENDER;
                                    tBL_TRACKNOTIF.DATE_NOTIF = tBL_NOTIF.DATE_NOTIF;
                                    tBL_TRACKNOTIF.DATE_ACTION = DateTime.Now;
                                    TBL_USER tb_userNOTIF1 = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
                                    TBL_TEACHER tb_teacherNOTIF1 = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_userNOTIF.FK_ID_TEACHER).FirstOrDefault();
                                    tBL_TRACKNOTIF.ACTION_NOTIF = "[EDIT USER] By " + tb_teacherNOTIF1.TEACHER_NAME;
                                    db.TBL_TRACKNOTIF.Add(tBL_TRACKNOTIF);
                                    db.SaveChanges();
                                    ts.Commit();
                                }

                                //db.SaveChanges();
                                //ts.Commit();
                                return RedirectToAction("Index");
                            }
                            catch (Exception)
                            {
                                ts.Rollback();
                                ViewData["data"] = "Something Error Please Try Again";
                            }
                        }
                    }
                    else
                    {
                        ViewData["homeroom"] = "Please First Add A Teacher Who Wants To Be Homeroom Teacher At Menu Manage Homeroom Teacher";
                    }
                }
                else
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {

                            String encrypted = code.Encrypt(tBL_USER.PASSWORD);
                            tBL_USER.PASSWORD = encrypted;
                            db.Entry(tBL_USER).State = EntityState.Modified;
                            db.SaveChanges();

                            int valuenotif1 = 0;
                            if (valuenotif1 == 0)
                            {
                                TBL_NOTIF tBL_NOTIF = new TBL_NOTIF();
                                tBL_NOTIF.INBOX = "Please Changes Your Password";
                                tBL_NOTIF.FK_ID_USER_RECEIVER = tBL_USER.ID_USER;
                                tBL_NOTIF.SENDER = tb_teacherNOTIF.TEACHER_NAME;
                                tBL_NOTIF.DATE_NOTIF = DateTime.Now;
                                db.TBL_NOTIF.Add(tBL_NOTIF);

                                TBL_TRACKNOTIF tBL_TRACKNOTIF = new TBL_TRACKNOTIF();
                                tBL_TRACKNOTIF.INBOX = tBL_NOTIF.INBOX;
                                tBL_TRACKNOTIF.FK_ID_USER_RECEIVER = tBL_NOTIF.FK_ID_USER_RECEIVER;
                                tBL_TRACKNOTIF.SENDER = tBL_NOTIF.SENDER;
                                tBL_TRACKNOTIF.DATE_NOTIF = tBL_NOTIF.DATE_NOTIF;
                                tBL_TRACKNOTIF.DATE_ACTION = DateTime.Now;
                                TBL_USER tb_userNOTIF1 = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
                                TBL_TEACHER tb_teacherNOTIF1 = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_userNOTIF.FK_ID_TEACHER).FirstOrDefault();
                                tBL_TRACKNOTIF.ACTION_NOTIF = "[EDIT USER] By " + tb_teacherNOTIF1.TEACHER_NAME;
                                db.TBL_TRACKNOTIF.Add(tBL_TRACKNOTIF);

                                db.SaveChanges();
                                ts.Commit();
                            }

                            //db.SaveChanges();
                            //ts.Commit();
                            return RedirectToAction("Index");
                        }
                        catch (Exception)
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                }

                    

            }
            ViewBag.POSITION = new SelectList(db.MS_ROLE, "ROLE", "ROLE", tBL_USER.POSITION);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_USER.FK_ID_TEACHER);
            return View(tBL_USER);
        }

        // GET: User/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_USER tBL_USER = db.TBL_USER.Find(id);
            if (tBL_USER == null)
            {
                return HttpNotFound();
            }
            return View(tBL_USER);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {

                    TBL_USER tBL_USER = db.TBL_USER.Find(id);
                    db.TBL_USER.Remove(tBL_USER);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
