//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PengRaSi
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBL_TRACKNOTIF
    {
        public int ID_TRACKNOTIF { get; set; }
        public string INBOX { get; set; }
        public string FK_ID_USER_RECEIVER { get; set; }
        public string SENDER { get; set; }
        public Nullable<System.DateTime> DATE_NOTIF { get; set; }
        public string ACTION_NOTIF { get; set; }
        public Nullable<System.DateTime> DATE_ACTION { get; set; }
    }
}
