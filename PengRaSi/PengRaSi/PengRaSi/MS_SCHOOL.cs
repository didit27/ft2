//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PengRaSi
{
    using System;
    using System.Collections.Generic;
    
    public partial class MS_SCHOOL
    {
        public int ID_SCHOOL { get; set; }
        public string SCHOOL_NAME { get; set; }
        public string SCHOOL_ADDRESS { get; set; }
        public byte[] SCHOOL_LOGO { get; set; }
    }
}
