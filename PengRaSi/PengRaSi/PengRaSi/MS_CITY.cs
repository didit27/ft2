//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PengRaSi
{
    using System;
    using System.Collections.Generic;
    
    public partial class MS_CITY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MS_CITY()
        {
            this.TBL_STUDENT = new HashSet<TBL_STUDENT>();
        }
    
        public int ID_CITY { get; set; }
        public string CITY { get; set; }
        public Nullable<int> FK_ID_PROVENCE { get; set; }
    
        public virtual MS_PROV MS_PROV { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TBL_STUDENT> TBL_STUDENT { get; set; }
    }
}
