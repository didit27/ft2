﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PengRaSi.App_Start
{
    public class SecretRouteHandler : MvcRouteHandler
    {
		protected override IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestCtx)
		{
			string key1 = requestCtx.RouteData.Values["enc1"].ToString();
			string key2 = requestCtx.RouteData.Values["enc2"].ToString();
			string row = requestCtx.RouteData.Values["rowid"].ToString();
			string op = requestCtx.RouteData.Values["operation"].ToString();

			// logic to validate keys valid for current user or other validations

			requestCtx.RouteData.Values["controller"] = "Home"; // Home not HomeController
			requestCtx.RouteData.Values["action"] = "Index";  // where the encoded keys should take the user

			// if invalid, could throw NotFound or other error, or return an unauthorized action instead of SomeActionName

			return base.GetHttpHandler(requestCtx);
		}
	}
}