﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static string connectionString = "SERVER = localhost; DATABASE = pengrasi; USER ID = sa; PASSWORD = 123; MultipleActiveResultSets=True";
        static SqlConnection sqlconn = new SqlConnection(connectionString);
        static void Main(string[] args)
        {
            using (sqlconn)
            {
                try
                {
                    sqlconn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT ANNOUNCEMENT FROM TBL_ANNOUNCE",sqlconn);
                    SqlDataReader reader = cmd.ExecuteReader();
                   
                    while (reader.Read())
                    {
                        string pesan = reader.GetValue(0).ToString();
                        SqlCommand cmdUser = new SqlCommand("SELECT ID_USER FROM TBL_USER", sqlconn);
                        SqlDataReader readerUser = cmdUser.ExecuteReader();
                        while(readerUser.Read())
                        {
                            string user = readerUser.GetValue(0).ToString();
                            if (user != "admin")
                            {
                                DateTime dt = DateTime.Now;
                                var dt1 = "yyyy/MM/dd HH:mm:ss";
                                SqlCommand addNotif = new SqlCommand("INSERT INTO TBL_NOTIF VALUES('" + pesan + "','" + user + "','admin','"+dt.ToString(dt1)+"')", sqlconn);
                                SqlCommand addTrack = new SqlCommand("INSERT INTO TBL_TRACKNOTIF VALUES('" + pesan + "','" + user + "','admin','" + dt.ToString(dt1) + "','[ANNOUNTCEMENT/RIMEDER] By admin','" + dt.ToString(dt1) + "')", sqlconn);
                                //SqlCommand addTrack = new SqlCommand("INSERT INTO TBL_TRACKNOTIF VALUES('" + pesan + "','""')", sqlconn);
                                addNotif.ExecuteNonQuery();
                                addTrack.ExecuteNonQuery();
                            }
                           
                        }
                    }
                    
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    Console.ReadKey();
                }
                sqlconn.Close();
            }

        }
    }
}
