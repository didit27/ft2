﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testPengrasi;

namespace testPengrasi.Controllers
{
    public class NotificationController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Notification
        public ActionResult Index()
        {
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            var tBL_NOTIF = db.TBL_NOTIF.Where(x=> x.FK_ID_USER_RECEIVER==tb_user.ID_USER).Include(t => t.TBL_USER).ToList();
            return View(tBL_NOTIF.ToList());
        }

        // GET: Notification/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_NOTIF tBL_NOTIF = db.TBL_NOTIF.Find(id);
            if (tBL_NOTIF == null)
            {
                return HttpNotFound();
            }
            return View(tBL_NOTIF);
        }

        // GET: Notification/Create
        public ActionResult Create()
        {
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            ViewBag.FK_ID_USER_RECEIVER = new SelectList(db.TBL_USER.Where(x=> x.ID_USER!=User.Identity.Name), "ID_USER", "ID_USER");
            return View();
        }

        // POST: Notification/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_NOTIF,INBOX,FK_ID_USER_RECEIVER,SENDER")] TBL_NOTIF tBL_NOTIF)
        {

            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {

                        TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
                        TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_user.FK_ID_TEACHER).FirstOrDefault();
                        if (tb_user.ID_USER == tBL_NOTIF.FK_ID_USER_RECEIVER)
                        {
                            ViewData["Nosamesender"] = "Sending Messages Can Not Be Self";
                        }
                        else
                        {
                            tBL_NOTIF.SENDER = tb_teacher.TEACHER_NAME;
                            db.TBL_NOTIF.Add(tBL_NOTIF);
                            db.SaveChanges();
                            ts.Commit();
                            return RedirectToAction("Index");
                        }
                    }
                    catch
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
               
            }

            ViewBag.FK_ID_USER_RECEIVER = new SelectList(db.TBL_USER.Where(x => x.ID_USER != User.Identity.Name), "ID_USER", "ID_USER", tBL_NOTIF.FK_ID_USER_RECEIVER);
            return View(tBL_NOTIF);
        }

        // GET: Notification/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_NOTIF tBL_NOTIF = db.TBL_NOTIF.Find(id);
            if (tBL_NOTIF == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_ID_USER_RECEIVER = new SelectList(db.TBL_USER, "ID_USER", "PASSWORD", tBL_NOTIF.FK_ID_USER_RECEIVER);
            return View(tBL_NOTIF);
        }

        // POST: Notification/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_NOTIF,INBOX,FK_ID_USER_RECEIVER,SENDER")] TBL_NOTIF tBL_NOTIF)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tBL_NOTIF).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_ID_USER_RECEIVER = new SelectList(db.TBL_USER, "ID_USER", "PASSWORD", tBL_NOTIF.FK_ID_USER_RECEIVER);
            return View(tBL_NOTIF);
        }

        // GET: Notification/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_NOTIF tBL_NOTIF = db.TBL_NOTIF.Find(id);
            if (tBL_NOTIF == null)
            {
                return HttpNotFound();
            }
            return View(tBL_NOTIF);
        }

        // POST: Notification/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_NOTIF tBL_NOTIF = db.TBL_NOTIF.Find(id);
                    db.TBL_NOTIF.Remove(tBL_NOTIF);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
