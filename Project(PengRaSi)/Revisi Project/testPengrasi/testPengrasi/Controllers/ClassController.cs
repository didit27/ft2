﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testPengrasi;
using System.Web.Security;


namespace testPengrasi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class ClassController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Class
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            var tBL_CLASS = db.TBL_CLASS.Include(t => t.MS_GRADE).Include(t => t.TBL_DEPT);
            return View(tBL_CLASS.ToList());
        }

        // GET: Class/Details/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_CLASS tBL_CLASS = db.TBL_CLASS.Find(id);
            if (tBL_CLASS == null)
            {
                return HttpNotFound();
            }
            return View(tBL_CLASS);
        }

        // GET: Class/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    
                    ViewBag.GRADE = new SelectList(db.MS_GRADE, "ID_GRADE", "Grade");
                    ViewBag.FK_ID_DEPT = new SelectList(db.TBL_DEPT, "ID_DEPT", "DEPT_NAME");
                }
                catch (Exception )
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }

            }
            return View();
        }

        // POST: Class/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create(TBL_CLASS tBL_CLASS)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        TBL_CLASS tb_Class = new TBL_CLASS();
                        TBL_DEPT tBL_DEPT = db.TBL_DEPT.Where(x => x.ID_DEPT == tBL_CLASS.FK_ID_DEPT).FirstOrDefault();
                        MS_GRADE grd = db.MS_GRADE.Where(x => x.ID_GRADE == tBL_CLASS.GRADE).FirstOrDefault();

                        tb_Class.ID_CLASS = tBL_CLASS.SCH_YEAR.ToString() + grd.GRADE.ToString() + tBL_DEPT.ID_DEPT + Request["classNo"].ToString();
                        tb_Class.CLASS_NAME = grd.GRADE + " " + tBL_DEPT.NAME_SHORT + " " + Request["classNo"].ToString();
                        tb_Class.GRADE = tBL_CLASS.GRADE;
                        tb_Class.SCH_YEAR = tBL_CLASS.SCH_YEAR;
                        tb_Class.FK_ID_DEPT = tBL_CLASS.FK_ID_DEPT;
                        db.TBL_CLASS.Add(tb_Class);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception )
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
                   
            }
            ViewBag.GRADE = new SelectList(db.MS_GRADE, "ID_GRADE", "Grade", tBL_CLASS.GRADE);
            ViewBag.SCH_YEAR = new SelectList(db.MS_SCH_YEAR, "ID_SCH_YEAR", "SCH_YEAR", tBL_CLASS.SCH_YEAR);
            ViewBag.FK_ID_DEPT = new SelectList(db.TBL_DEPT, "ID_DEPT", "DEPT_NAME", tBL_CLASS.FK_ID_DEPT);
            return View(tBL_CLASS);
        }

        // GET: Class/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(string id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_CLASS tBL_CLASS = db.TBL_CLASS.Find(id);
            if (tBL_CLASS == null)
            {
                return HttpNotFound();
            }
            ViewBag.GRADE = new SelectList(db.MS_GRADE, "ID_GRADE", "Grade", tBL_CLASS.GRADE);
            ViewBag.SCH_YEAR = new SelectList(db.MS_SCH_YEAR, "ID_SCH_YEAR", "SCH_YEAR", tBL_CLASS.SCH_YEAR);
            ViewBag.FK_ID_DEPT = new SelectList(db.TBL_DEPT, "ID_DEPT", "DEPT_NAME", tBL_CLASS.FK_ID_DEPT);
            ViewBag.classNo = tBL_CLASS.CLASS_NAME.Substring(tBL_CLASS.CLASS_NAME.Length-1);
            return View(tBL_CLASS);
        }

        // POST: Class/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "ID_CLASS,CLASS_NAME,GRADE,SCH_YEAR,FK_ID_DEPT")] TBL_CLASS tBL_CLASS)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        TBL_CLASS tb_Class = new TBL_CLASS();
                        TBL_DEPT tBL_DEPT = db.TBL_DEPT.Where(x => x.ID_DEPT == tBL_CLASS.FK_ID_DEPT).FirstOrDefault();
                        MS_SCH_YEAR sch = db.MS_SCH_YEAR.Where(x => x.ID_SCH_YEAR == tBL_CLASS.SCH_YEAR).FirstOrDefault();
                        MS_GRADE grd = db.MS_GRADE.Where(x => x.ID_GRADE == tBL_CLASS.GRADE).FirstOrDefault();

                        tb_Class.ID_CLASS = tb_Class.ID_CLASS;
                        tb_Class.CLASS_NAME = grd.GRADE + " " + tBL_DEPT.NAME_SHORT + " " + Request["classNo"].ToString();
                        tb_Class.GRADE = tBL_CLASS.GRADE;
                        tb_Class.SCH_YEAR = tBL_CLASS.SCH_YEAR;
                        tb_Class.FK_ID_DEPT = tBL_CLASS.FK_ID_DEPT;

                        db.Entry(tb_Class).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception )
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }   
            }
            ViewBag.GRADE = new SelectList(db.MS_GRADE, "ID_GRADE", "Grade", tBL_CLASS.GRADE);
            ViewBag.SCH_YEAR = new SelectList(db.MS_SCH_YEAR, "ID_SCH_YEAR", "SCH_YEAR", tBL_CLASS.SCH_YEAR);
            ViewBag.FK_ID_DEPT = new SelectList(db.TBL_DEPT, "ID_DEPT", "DEPT_NAME", tBL_CLASS.FK_ID_DEPT);
            return View(tBL_CLASS);
        }

        // GET: Class/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_CLASS tBL_CLASS = db.TBL_CLASS.Find(id);
            if (tBL_CLASS == null)
            {
                return HttpNotFound();
            }
            return View(tBL_CLASS);
        }

        // POST: Class/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(string id)
        {

            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_CLASS tBL_CLASS = db.TBL_CLASS.Find(id);
                    db.TBL_CLASS.Remove(tBL_CLASS);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception )
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();
        }
        [Authorize(Roles = "Administrator")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
