﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testPengrasi;

namespace testPengrasi.Controllers
{
    [Authorize]
    public class DepartmentController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Department
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View(db.TBL_DEPT.ToList());
        }

        // GET: Department/Details/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Details(string id)
        {
           

                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    TBL_DEPT tBL_DEPT = db.TBL_DEPT.Find(id);
                    if (tBL_DEPT == null)
                    {
                        return HttpNotFound();
                    }
        
            
            return View(tBL_DEPT);
        }

        // GET: Department/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Department/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create([Bind(Include = "ID_DEPT,DEPT_NAME,NAME_SHORT")] TBL_DEPT tBL_DEPT)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        TBL_DEPT tBL_DEPTNew = new TBL_DEPT();
                        tBL_DEPTNew.ID_DEPT = tBL_DEPT.NAME_SHORT;
                        tBL_DEPTNew.DEPT_NAME = tBL_DEPT.DEPT_NAME;
                        tBL_DEPTNew.NAME_SHORT = tBL_DEPT.NAME_SHORT;
                        db.TBL_DEPT.Add(tBL_DEPTNew);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }

                }
            }

            return View(tBL_DEPT);
        }

        // GET: Department/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_DEPT tBL_DEPT = db.TBL_DEPT.Find(id);
            if (tBL_DEPT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_DEPT);
        }

        // POST: Department/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "ID_DEPT,DEPT_NAME,NAME_SHORT")] TBL_DEPT tBL_DEPT)
        {

            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        TBL_DEPT tBL_DEPTNew = new TBL_DEPT();
                        tBL_DEPTNew.ID_DEPT = tBL_DEPT.NAME_SHORT;
                        tBL_DEPTNew.DEPT_NAME = tBL_DEPT.DEPT_NAME;
                        tBL_DEPTNew.NAME_SHORT = tBL_DEPT.NAME_SHORT;
                       
                        db.Entry(tBL_DEPTNew).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch(Exception )
                    {

                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }     
            }
            return View();
        }

        // GET: Department/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_DEPT tBL_DEPT = db.TBL_DEPT.Find(id);
            if (tBL_DEPT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_DEPT);
        }

        // POST: Department/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_DEPT tBL_DEPT = db.TBL_DEPT.Find(id);
                    db.TBL_DEPT.Remove(tBL_DEPT);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch(Exception )
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
          return View();    
        }
        [Authorize(Roles = "Administrator")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
