﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testPengrasi;


namespace testPengrasi.Controllers
{
    [Authorize]
    
    public class StudentController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Student
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            var tBL_STUDENT = db.TBL_STUDENT.Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);
            
            return View(tBL_STUDENT.ToList());
        }

        // GET: Student/Details/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // GET: Student/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER");
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION");
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME");

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create([Bind(Include = "STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {
           
            string a = tBL_STUDENT.ID_STUDENT;
            string b = tBL_STUDENT.NIS;
            string c = tBL_STUDENT.FK_ID_CLASS;
            TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            TBL_CLASS tbl_studentyear = db.TBL_CLASS.Where(x => x.ID_CLASS == c).FirstOrDefault();

            var tbvalidasi = db.TBL_STUDENT.Where(x => x.NIS == b && x.TBL_CLASS.SCH_YEAR==tbl_studentyear.SCH_YEAR).FirstOrDefault();
       
          


            if (tbl_studentvalidasiid == null)
            {
                if (tbvalidasi!=null)
                {
                    ViewData["nissame"] = "NIS Same";
                   
                }
                else
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                            db.TBL_STUDENT.Add(tBL_STUDENT);
                            db.SaveChanges();
                            ts.Commit();
                            return RedirectToAction("Index");
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }
                    
                }

            }
            else
            {
                ViewData["idstudentsama"] = "Id Student Same";
            }
            //if (ModelState.IsValid)
            //{
            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0,2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length-4),4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);
            return View(tBL_STUDENT);
        }

        // GET: Student/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View(tBL_STUDENT);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "ID_STUDENT,STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {
            ////string a = tBL_STUDENT.ID_STUDENT;
            ////string b = tBL_STUDENT.NIS;
            ////TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis2 = db.TBL_STUDENT.Where(x => x.NIS == tbl_studentvalidasiid.NIS).FirstOrDefault();
            ////string d = tbl_studentvalidasinis2.NIS;
            ////string c = tBL_STUDENT.NIS;
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {

                    ////TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
                    ////db.TBL_STUDENT.Remove(tBL1_STUDENT);
                    ////db.SaveChanges();


                    ////if (tbl_studentvalidasinis == null || c == d)
                    ////{
                    ////    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                    ////    db.TBL_STUDENT.Add(tBL_STUDENT);
                    ////    db.SaveChanges();
                    ////    ts.Commit();
                    ////    return RedirectToAction("Index");
                    ////}
                    ////else
                    ////{
                    ////    ViewData["data"] = "Something Error Or NIS Same";
                    ////}

                    db.Entry(tBL_STUDENT).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Or NIS Same";
                }
            }

            //if (ModelState.IsValid)
            //{
            //    TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
            //    db.TBL_STUDENT.Remove(tBL1_STUDENT);
            //    db.SaveChanges();

            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    //db.Entry(tBL_STUDENT).State = EntityState.Modified;
            //    //db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);
            return View(tBL_STUDENT);
        }

        // GET: Student/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
                    db.TBL_STUDENT.Remove(tBL_STUDENT);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error ";
                }
            }
            return View();
        }

        //----------------------------HOOMROOM TEACHER-------------------)
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult IndexHT()
        {

            var a = System.Web.HttpContext.Current.User.Identity.Name;
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_user.FK_ID_TEACHER).FirstOrDefault();
            TBL_HOMEROOM tb_homeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tb_teacher.ID_TEACHER).FirstOrDefault();
            TBL_CLASS tb_class = db.TBL_CLASS.Where(x => x.ID_CLASS == tb_homeroom.FK_ID_CLASS).FirstOrDefault();
            TBL_STUDENT tb_student = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tb_class.ID_CLASS).FirstOrDefault();

            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tb_student.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);

            return View(tBL_STUDENT.ToList());
        }

        // GET: Student/Details/5
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult DetailsHT(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // GET: Student/Create
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult CreateHT()
        {
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER");
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION");
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME");

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult CreateHT([Bind(Include = "STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {

            //string a = tBL_STUDENT.ID_STUDENT;
            //string b = tBL_STUDENT.NIS;
            //TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            //TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();


            ////var a = System.Web.HttpContext.Current.User.Identity.Name;
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_user.FK_ID_TEACHER).FirstOrDefault();
            TBL_HOMEROOM tb_homeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tb_teacher.ID_TEACHER).FirstOrDefault();
            TBL_CLASS tb_class = db.TBL_CLASS.Where(x => x.ID_CLASS == tb_homeroom.FK_ID_CLASS).FirstOrDefault();
            //TBL_STUDENT tb_student = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tb_class.ID_CLASS).FirstOrDefault();
            tBL_STUDENT.FK_ID_CLASS = tb_class.ID_CLASS;
            string a = tBL_STUDENT.ID_STUDENT;
            string b = tBL_STUDENT.NIS;
            string c = tBL_STUDENT.FK_ID_CLASS;
            TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            TBL_CLASS tbl_studentyear = db.TBL_CLASS.Where(x => x.ID_CLASS == c).FirstOrDefault();

            var tbvalidasi = db.TBL_STUDENT.Where(x => x.NIS == b && x.TBL_CLASS.SCH_YEAR == tbl_studentyear.SCH_YEAR).FirstOrDefault();


            if (tbl_studentvalidasiid == null)
            {
                if (tbvalidasi != null)
                {
                    ViewData["nissame"] = "NIS Same";

                }
                else
                {
                    using (DbContextTransaction ts = db.Database.BeginTransaction())
                    {
                        try
                        {
                            tBL_STUDENT.FK_ID_CLASS = tb_class.ID_CLASS;
                            tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                            db.TBL_STUDENT.Add(tBL_STUDENT);
                            db.SaveChanges();
                            ts.Commit();
                            return RedirectToAction("IndexHT");
                        }
                        catch
                        {
                            ts.Rollback();
                            ViewData["data"] = "Something Error Please Try Again";
                        }
                    }

                }

            }
            else
            {
                ViewData["idstudentsama"] = "Id Student Same";
            }


            //if (tbl_studentvalidasiid == null)
            //{
            //    if (tbl_studentvalidasinis == null)
            //    {
            //        using (DbContextTransaction ts = db.Database.BeginTransaction())
            //        {
            //            try
            //            {
            //                tBL_STUDENT.FK_ID_CLASS = tb_class.ID_CLASS;
            //                tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                            
            //                db.TBL_STUDENT.Add(tBL_STUDENT);
            //                db.SaveChanges();
            //                ts.Commit();
            //                return RedirectToAction("IndexHT");
            //            }
            //            catch
            //            {
            //                ts.Rollback();
            //                ViewData["data"] = "Something Error Please Try Again";
            //            }
            //        }
            //    }
            //    else
            //    {
            //        ViewData["nissame"] = "NIS Same";
            //    }

            //}
            //else
            //{
            //    ViewData["idstudentsama"] = "Id Student Same";
            //}
            //if (ModelState.IsValid)
            //{
            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0,2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length-4),4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);
            return View(tBL_STUDENT);
        }

        // GET: Student/Edit/5
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult EditHT(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);

            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.ClassesList = new SelectList(Classes, "Value", "Text");
            return View(tBL_STUDENT);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult EditHT([Bind(Include = "ID_STUDENT,STUDENT_NAME,NIS,BIRTHPLACE,BIRTHDATE,GENDER,RELIGION,ST_FAMILY,CHILD_ORDER,ADDRESS,NO_TLP,PREV_SCH,GRADE_ENTER,DATE_ENTER,FATHER_NAME,MOTHER_NAME,PARENT_ADDR,NO_TLP_PARENT,FATHER_JOB,MOTHER_JOB,GUARD_NAME,NO_TLP_GUARD,GUARD_ADDR,GUARD_JOB,FK_ID_CLASS")] TBL_STUDENT tBL_STUDENT)
        {
            ////string a = tBL_STUDENT.ID_STUDENT;
            ////string b = tBL_STUDENT.NIS;
            ////TBL_STUDENT tbl_studentvalidasiid = db.TBL_STUDENT.Where(x => x.ID_STUDENT == a).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis = db.TBL_STUDENT.Where(x => x.NIS == b).FirstOrDefault();
            ////TBL_STUDENT tbl_studentvalidasinis2 = db.TBL_STUDENT.Where(x => x.NIS == tbl_studentvalidasiid.NIS).FirstOrDefault();
            ////string d = tbl_studentvalidasinis2.NIS;
            ////string c = tBL_STUDENT.NIS;
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {

                    ////TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
                    ////db.TBL_STUDENT.Remove(tBL1_STUDENT);
                    ////db.SaveChanges();


                    ////if (tbl_studentvalidasinis == null || c == d)
                    ////{
                    ////    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
                    ////    db.TBL_STUDENT.Add(tBL_STUDENT);
                    ////    db.SaveChanges();
                    ////    ts.Commit();
                    ////    return RedirectToAction("IndexHT");
                    ////}
                    ////else
                    ////{
                    ////    ViewData["data"] = "Something Error Or NIS Same";
                    ////}


                    db.Entry(tBL_STUDENT).State = EntityState.Modified;
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("IndexHT");

                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Or NIS Same";
                }
            }

            //if (ModelState.IsValid)
            //{
            //    TBL_STUDENT tBL1_STUDENT = db.TBL_STUDENT.Find(tBL_STUDENT.ID_STUDENT);
            //    db.TBL_STUDENT.Remove(tBL1_STUDENT);
            //    db.SaveChanges();

            //    tBL_STUDENT.ID_STUDENT = tBL_STUDENT.FK_ID_CLASS.Remove(0, 2) + tBL_STUDENT.NIS.Substring((tBL_STUDENT.NIS.Length - 4), 4);
            //    db.TBL_STUDENT.Add(tBL_STUDENT);
            //    db.SaveChanges();
            //    //db.Entry(tBL_STUDENT).State = EntityState.Modified;
            //    //db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            ViewBag.GENDER = new SelectList(db.MS_GENDER, "ID_GENDER", "GENDER", tBL_STUDENT.GENDER);
            ViewBag.RELIGION = new SelectList(db.MS_RELIGION, "ID_RELIGION", "RELIGION", tBL_STUDENT.RELIGION);
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_STUDENT.FK_ID_CLASS);
            return View(tBL_STUDENT);
        }

        // GET: Student/Delete/5
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult DeleteHT(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
            if (tBL_STUDENT == null)
            {
                return HttpNotFound();
            }
            return View(tBL_STUDENT);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("DeleteHT")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Homeroom Teacher")]
        public ActionResult DeleteConfirmedHT(string id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    TBL_STUDENT tBL_STUDENT = db.TBL_STUDENT.Find(id);
                    db.TBL_STUDENT.Remove(tBL_STUDENT);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("IndexHT");
                }
                catch
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error ";
                }
            }
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult IndexTeacher(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
            var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();
            
            var tBL_STUDENT = db.TBL_STUDENT.Where(x=> x.FK_ID_CLASS==tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


            var tb_Student = db.StudentScores.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

            ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
            ViewData["Class"] = tBL_CLASS.CLASS_NAME;
            ViewData["SubjClass"] = id;

            return View(tb_Student);
        }


        public ActionResult IndexTeacherScoreAtitude(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
            var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


            var tb_Student = db.StudentAttitudes.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

            ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
            ViewData["Class"] = tBL_CLASS.CLASS_NAME;
            ViewData["SubjClass"] = id;

            return View(tb_Student);
        }

        public ActionResult IndexTeacherScorePractices(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
            var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


            var tb_Student = db.StudentPractices.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

            ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
            ViewData["Class"] = tBL_CLASS.CLASS_NAME;
            ViewData["SubjClass"] = id;

            return View(tb_Student);
        }

        public ActionResult IndexHRScoreTheory(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
            var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


            var tb_Student = db.StudentScores.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

            ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
            ViewData["Class"] = tBL_CLASS.CLASS_NAME;
            ViewData["SubjClass"] = id;

            return View(tb_Student);
        }

        public ActionResult IndexHRScoreAtitude(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
            var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


            var tb_Student = db.StudentAttitudes.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

            ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
            ViewData["Class"] = tBL_CLASS.CLASS_NAME;
            ViewData["SubjClass"] = id;

            return View(tb_Student);
        }
        public ActionResult IndexHRScorePractices(int id)
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Where(x => x.ID_SUBJECT_CLASS == id).FirstOrDefault();
            var tBL_CLASS = db.TBL_CLASS.Where(x => x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).FirstOrDefault();
            var tBL_SUBJECT = db.TBL_SUBJECT.Where(x => x.ID_SUBJECT == tBL_SUBJECT_CLASS.FK_ID_SUBJECT).FirstOrDefault();

            var tBL_STUDENT = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS).Include(t => t.MS_GENDER).Include(t => t.MS_RELIGION).Include(t => t.TBL_CLASS);


            var tb_Student = db.StudentPractices.Where(x => x.ID_SUBJECT_CLASS == id && x.ID_CLASS == tBL_SUBJECT_CLASS.FK_ID_CLASS);

            ViewData["Subject"] = tBL_SUBJECT.SUBJECT_NAME;
            ViewData["Class"] = tBL_CLASS.CLASS_NAME;
            ViewData["SubjClass"] = id;

            return View(tb_Student);
        }

    }
}
