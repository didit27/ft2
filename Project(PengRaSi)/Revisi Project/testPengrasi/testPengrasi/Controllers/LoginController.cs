﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace testPengrasi.Controllers
{

    public class LoginController : Controller
    {
        public pengrasiEntities DB = new pengrasiEntities();
        Models.Encryption code = new Models.Encryption();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
       
        [HttpPost]
        public ActionResult Index(TBL_USER model)
        {
           
            using (DbContextTransaction ts = DB.Database.BeginTransaction())
            {
                try
                {
                    using (DB)
                    {
                        
                        string encPass = code.Encrypt(model.PASSWORD);
                        bool isValid = DB.TBL_USER.Any(x => x.ID_USER == model.ID_USER && x.PASSWORD == encPass);
                       
                        if (isValid)
                        {
                            pengrasiEntities dbnav1 = new pengrasiEntities();
                            TBL_USER tb_user = dbnav1.TBL_USER.Where(x => x.ID_USER == model.ID_USER).FirstOrDefault();
                            if (tb_user.POSITION == "Administrator")
                            {
                                FormsAuthentication.SetAuthCookie(model.ID_USER, false);
                                return RedirectToAction("Index", "Home");
                            }
                            if (tb_user.POSITION == "Homeroom Teacher")
                            {
                                FormsAuthentication.SetAuthCookie(model.ID_USER, false);
                                return RedirectToAction("HomeroomTeacher", "Test");
                            }
                            if (tb_user.POSITION == "Teacher")
                            {
                                FormsAuthentication.SetAuthCookie(model.ID_USER, false);
                                return RedirectToAction("teacher", "Test");
                            }

                        }
                        ModelState.AddModelError("", "Invalid Username or Password");

                    }
                }
                catch(Exception)
                {
                   // ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            /*using (DB)
            {
                string encPass = code.Encrypt(model.PASSWORD);
                bool isValid = DB.TBL_USER.Any(x=>x.ID_USER == model.ID_USER && x.PASSWORD == encPass);
                if(isValid)
                {
                    FormsAuthentication.SetAuthCookie(model.ID_USER, false);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Invalid Username or Password");

            } */
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }

        [Authorize]
        public ActionResult ChangePass()
        {
            var id = User.Identity.Name;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_USER tBL_USER = DB.TBL_USER.Find(id);
            if (tBL_USER == null)
            {
                return HttpNotFound();
            }

            return View(tBL_USER);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePass([Bind(Include = "ID_USER,PASSWORD,POSITION,FK_ID_TEACHER")] TBL_USER tBL_USER)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = DB.Database.BeginTransaction())
                {
                    try
                    {
                        String encrypted = code.Encrypt(tBL_USER.PASSWORD);
                        tBL_USER.PASSWORD = encrypted;
                        DB.Entry(tBL_USER).State = EntityState.Modified;
                        DB.SaveChanges();
                        ts.Commit();
                        ViewData["data"] = "Password change success, please login";
                        return View();
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }

            }
            return View(tBL_USER);
        }
    }
}