﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testPengrasi;

namespace testPengrasi.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class ReligionController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: Religion
        public ActionResult Index()
        {
            return View(db.MS_RELIGION.ToList());
        }

        // GET: Religion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_RELIGION mS_RELIGION = db.MS_RELIGION.Find(id);
            if (mS_RELIGION == null)
            {
                return HttpNotFound();
            }
            return View(mS_RELIGION);
        }

        // GET: Religion/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Religion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_RELIGION,RELIGION,INFORMATION")] MS_RELIGION mS_RELIGION)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.MS_RELIGION.Add(mS_RELIGION);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch(Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
            }

            return View(mS_RELIGION);
        }

        // GET: Religion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_RELIGION mS_RELIGION = db.MS_RELIGION.Find(id);
            if (mS_RELIGION == null)
            {
                return HttpNotFound();
            }
            return View(mS_RELIGION);
        }

        // POST: Religion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_RELIGION,RELIGION,INFORMATION")] MS_RELIGION mS_RELIGION)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(mS_RELIGION).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        ts.Rollback();
                        ViewData["data"] = "Something Error Please Try Again";
                    }
                }
            }
            return View(mS_RELIGION);
        }

        // GET: Religion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_RELIGION mS_RELIGION = db.MS_RELIGION.Find(id);
            if (mS_RELIGION == null)
            {
                return HttpNotFound();
            }
            return View(mS_RELIGION);
        }

        // POST: Religion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    MS_RELIGION mS_RELIGION = db.MS_RELIGION.Find(id);
                    db.MS_RELIGION.Remove(mS_RELIGION);
                    db.SaveChanges();
                    ts.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
