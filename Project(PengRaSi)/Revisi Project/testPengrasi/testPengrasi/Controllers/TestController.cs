﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace testPengrasi.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        [Authorize(Roles = "Teacher,Homeroom Teacher,Administrator")]
        public ActionResult Teacher()
        {
            return View();
        }
        [Authorize(Roles = "Homeroom Teacher,Administrator")]
        public ActionResult HomeroomTeacher()
        {
            return View();
        }
    }
}