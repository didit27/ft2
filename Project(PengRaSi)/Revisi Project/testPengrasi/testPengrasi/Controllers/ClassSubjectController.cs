﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testPengrasi;

namespace testPengrasi.Controllers
{
    [Authorize]
    public class ClassSubjectController : Controller
    {
        private pengrasiEntities db = new pengrasiEntities();

        // GET: ClassSubject
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Include(t => t.TBL_CLASS).Include(t => t.TBL_SUBJECT).Include(t => t.TBL_TEACHER);
            return View(tBL_SUBJECT_CLASS.ToList());
        }

        // GET: ClassSubject/Details/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SUBJECT_CLASS tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Find(id);
            if (tBL_SUBJECT_CLASS == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SUBJECT_CLASS);
        }

        // GET: ClassSubject/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.FK_ID_CLASS = new SelectList(Classes, "Value", "Text");
            ViewBag.FK_ID_SUBJECT = new SelectList(db.TBL_SUBJECT, "ID_SUBJECT", "SUBJECT_NAME");
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER.Where(x => x.TEACHER_NAME != "ADMIN"), "ID_TEACHER", "TEACHER_NAME");
            return View();
        }

        // POST: ClassSubject/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_SUBJECT_CLASS,FK_ID_SUBJECT,FK_ID_TEACHER,FK_ID_CLASS,SMT")] TBL_SUBJECT_CLASS tBL_SUBJECT_CLASS)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.TBL_SUBJECT_CLASS.Add(tBL_SUBJECT_CLASS);
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_SUBJECT_CLASS.FK_ID_CLASS);
            ViewBag.FK_ID_SUBJECT = new SelectList(db.TBL_SUBJECT, "ID_SUBJECT", "SUBJECT_NAME", tBL_SUBJECT_CLASS.FK_ID_SUBJECT);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER.Where(x => x.TEACHER_NAME != "ADMIN"), "ID_TEACHER", "TEACHER_NAME", tBL_SUBJECT_CLASS.FK_ID_TEACHER);
            return View(tBL_SUBJECT_CLASS);
        }

        // GET: ClassSubject/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SUBJECT_CLASS tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Find(id);
            if (tBL_SUBJECT_CLASS == null)
            {
                return HttpNotFound();
            }
            var Classes = db.TBL_CLASS.Select(s => new
            {
                Text = s.CLASS_NAME + " " + s.SCH_YEAR,
                Value = s.ID_CLASS
            }).ToList();
            ViewBag.FK_ID_CLASS = new SelectList(Classes, "Value", "Text", tBL_SUBJECT_CLASS.FK_ID_CLASS);
            ViewBag.FK_ID_SUBJECT = new SelectList(db.TBL_SUBJECT, "ID_SUBJECT", "SUBJECT_NAME", tBL_SUBJECT_CLASS.FK_ID_SUBJECT);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_SUBJECT_CLASS.FK_ID_TEACHER);
            return View(tBL_SUBJECT_CLASS);
        }

        // POST: ClassSubject/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_SUBJECT_CLASS,FK_ID_SUBJECT,FK_ID_TEACHER,FK_ID_CLASS,SMT")] TBL_SUBJECT_CLASS tBL_SUBJECT_CLASS)
        {
            using (DbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.Entry(tBL_SUBJECT_CLASS).State = EntityState.Modified;
                        db.SaveChanges();
                        ts.Commit();
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    ts.Rollback();
                    ViewData["data"] = "Something Error Please Try Again";
                }
            }
            ViewBag.FK_ID_CLASS = new SelectList(db.TBL_CLASS, "ID_CLASS", "CLASS_NAME", tBL_SUBJECT_CLASS.FK_ID_CLASS);
            ViewBag.FK_ID_SUBJECT = new SelectList(db.TBL_SUBJECT, "ID_SUBJECT", "SUBJECT_NAME", tBL_SUBJECT_CLASS.FK_ID_SUBJECT);
            ViewBag.FK_ID_TEACHER = new SelectList(db.TBL_TEACHER, "ID_TEACHER", "TEACHER_NAME", tBL_SUBJECT_CLASS.FK_ID_TEACHER);
            return View(tBL_SUBJECT_CLASS);
        }

        // GET: ClassSubject/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SUBJECT_CLASS tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Find(id);
            if (tBL_SUBJECT_CLASS == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SUBJECT_CLASS);
        }

        // POST: ClassSubject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(int id)
        {
            TBL_SUBJECT_CLASS tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Find(id);
            db.TBL_SUBJECT_CLASS.Remove(tBL_SUBJECT_CLASS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        //Subject for teacher
        public ActionResult IndexTeach()
        {
            var tbGetTeachId = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).FirstOrDefault();
            var tbGetTeacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tbGetTeachId.FK_ID_TEACHER).FirstOrDefault();
            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Include(t => t.TBL_CLASS).Include(t => t.TBL_SUBJECT).Include(t => t.TBL_TEACHER).Where(x=>x.FK_ID_TEACHER == tbGetTeacher.ID_TEACHER);
            ViewData["Teacher"] = tbGetTeacher.TEACHER_NAME;
            return View(tBL_SUBJECT_CLASS.ToList());
        }

        public ActionResult IndexClassSubjectHomeroom()
        {
            //var tbGetTeachId = db.TBL_USER.Where(x => x.ID_USER == User.Identity.Name).FirstOrDefault();
            //var tbGetTeacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tbGetTeachId.FK_ID_TEACHER).FirstOrDefault();
            //var tbGetHoomroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_CLASS == tbGetTeacher.ID_TEACHER).FirstOrDefault();
            //var tbGetClass = db.TBL_CLASS.Where(x => x.ID_CLASS == tbGetHoomroom.FK_ID_CLASS).FirstOrDefault();


            var a = System.Web.HttpContext.Current.User.Identity.Name;
            TBL_USER tb_user = db.TBL_USER.Where(x => x.ID_USER == @User.Identity.Name).FirstOrDefault();
            TBL_TEACHER tb_teacher = db.TBL_TEACHER.Where(x => x.ID_TEACHER == tb_user.FK_ID_TEACHER).FirstOrDefault();
            TBL_HOMEROOM tb_homeroom = db.TBL_HOMEROOM.Where(x => x.FK_ID_TEACHER == tb_teacher.ID_TEACHER).FirstOrDefault();
            TBL_CLASS tb_class = db.TBL_CLASS.Where(x => x.ID_CLASS == tb_homeroom.FK_ID_CLASS).FirstOrDefault();
            TBL_STUDENT tb_student = db.TBL_STUDENT.Where(x => x.FK_ID_CLASS == tb_class.ID_CLASS).FirstOrDefault();




            var tBL_SUBJECT_CLASS = db.TBL_SUBJECT_CLASS.Include(t => t.TBL_CLASS).Include(t => t.TBL_SUBJECT).Include(t => t.TBL_TEACHER).Where(x => x.FK_ID_CLASS == tb_class.ID_CLASS);
            ViewData["Teacher"] = tb_teacher.TEACHER_NAME;
            return View(tBL_SUBJECT_CLASS.ToList());
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
