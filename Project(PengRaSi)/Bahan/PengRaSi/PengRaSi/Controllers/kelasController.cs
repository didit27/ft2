﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PengRaSi;

namespace PengRaSi.Controllers
{
    public class kelasController : Controller
    {
        private pengrasiEntitiesKelas db = new pengrasiEntitiesKelas();

        // GET: kelas
        public ActionResult Index()
        {
            return View(db.kelas.ToList());
        }

        // GET: kelas/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kelas kelas = db.kelas.Find(id);
            if (kelas == null)
            {
                return HttpNotFound();
            }
            return View(kelas);
        }

        // GET: kelas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: kelas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_kelas,kelas1,jurusan")] kelas kelas)
        {
            if (ModelState.IsValid)
            {
                db.kelas.Add(kelas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kelas);
        }

        // GET: kelas/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kelas kelas = db.kelas.Find(id);
            if (kelas == null)
            {
                return HttpNotFound();
            }
            return View(kelas);
        }

        // POST: kelas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_kelas,kelas1,jurusan")] kelas kelas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kelas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kelas);
        }

        // GET: kelas/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kelas kelas = db.kelas.Find(id);
            if (kelas == null)
            {
                return HttpNotFound();
            }
            return View(kelas);
        }

        // POST: kelas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            kelas kelas = db.kelas.Find(id);
            db.kelas.Remove(kelas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
